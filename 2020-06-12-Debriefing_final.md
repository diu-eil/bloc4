DIU EIL Bloc 4 : débriefing général du bloc du vendredi 12 juin 2020
====================================================================


OdJ
-----

* Bilan du bloc
* Examen
* FOAD
* Question diverses

Bilan du bloc
-------------

- mattermost : faire de l'éditorial, avoir plus de canaux (e.g., un par sujet de TD/TP)
- modélisation BD : modélisation de la cave à vins trop ouverte, nécessité de l'entité faible, rencentrer sur la compétence 'comprendre un schéma'
- séances du matin (débriefing post travail personnel) : mieux les cadrer, par exemple en reprenant un corrigé, en préparant des questions/tours de paroles en amont
- positif : le mode "classe inversée" avec les vidéos, les sujets en amont permettent de s'approprier les contenus
- positif : les exos "pour aller plus loin" et la difficulté explicitée, les canevas/trame des sujets, les éléments de vérification et tests
- **TODO** : une base de simplement chainée pour les structures, pour apréhender plus facilement la POO

Examen
------

- durée 1h à 1h30 en temps limité à faire sur une plage d'ouverture 9h-17h le vendredi 19/6
  * on ouvrira un canal mattermost pour les soucis et cas particuliers
  * on fera un test de la plate-forme dans la semaine
- questions de type QROC (question réponses ouvertes mais courtes) et éventuellement un peu de QCM
- pour la partie structure de données, les questions porteront en partie sur une nouvelle structure, e.g., la liste circulaire
- programme:
  * produire des lambdas : non, en comprendre des simples oui
  * faire du Pythonic : non, mais qualité au sens général : oui
  * conception et E/A : analyse et compréhension : oui, production : non
  * formes normales autres que FNBC : non
  * POO : comme on l'a pratiquée sur les Structures de Données : oui, mais héritage, surcharge et conception OO : non
     
     
FOAD
----

- modalités : 10h de travail pour produire 1,5h de contenus pour les meilleurs élèves _per capita_ dans le groupe
- critères : qualité technique, ambition (pour vous, pas les élèves), prise de recul
- remarques sur sujets type BD : on prend n'importe quel jeu de données réel et il y a du travail (collecte, transformation de format, volumétrie/performance, redondances, données manquantes etc) et des possibilités de croiser (e.g., code postal et bases des communes du Grand Lyon)
- modalités du rendu et partage entre stagiaires
  * chacun donne une URL sous son contrôle dans le pad (zip sur plateforme d'échange, projet git, etc.)
  * le projet doit être portable (linux, windows et mac) : privilégier du Python et SQLite _standard_
  * formats de fichiers **ouverts et portables** : pdf/libreoffice/MD
  * faire un canal Mattermost par groupe pour discuter entre vous et avec les formateurs

Questions diverses
------------------

- une séance plénière d'échange et de retours en présentiel à l'automne/hiver ?
   - OK sur le principe, on essaiera et on le proposera à l'IPR


