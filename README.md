# DIU-EIL Bloc 4 : programmation avancée et bases de données

Ensemble des informations dont les supports pédagogiques du bloc 4 du [Diplôme Inter-Universitaire "Enseigner l'informatique au lycée"](https://diu-eil.univ-lyon1.fr/) à l'Université Claude Bernard Lyon 1.

- [DIU-EIL Bloc 4 : programmation avancée et bases de données](#diu-eil-bloc-4--programmation-avancée-et-bases-de-données)
  - [Programme](#programme)
    - [Programme national du DIU](#programme-national-du-diu)
    - [Programme détaillé du bloc 4](#programme-détaillé-du-bloc-4)
  - [Calendrier](#calendrier)
    - [Phase préparatoire](#phase-préparatoire)
    - [Semaine du 7 juin](#semaine-du-7-juin)
      - [Remarques](#remarques)
    - [Modalités de contrôle des connaissances](#modalités-de-contrôle-des-connaissances)
  - [Logiciels utilisés](#logiciels-utilisés)
    - [Pour le distanciel](#pour-le-distanciel)
    - [Pour les enseignements](#pour-les-enseignements)
  - [Références](#références)
    - [Programmes NSI et DIU](#programmes-nsi-et-diu)
    - [Ressources autres DIU-EIL](#ressources-autres-diu-eil)
    - [Ouvrages de référence](#ouvrages-de-référence)
    - [Programmation Python](#programmation-python)

## Programme

### Programme national du DIU

> Ce bloc, qui concerne principalement le programme NSI de terminale, aborde d'une part quelques concepts avancés de programmation dont les types de données abstraits, et d'autre part les bases de données relationnelles, du point de vue des modèles, de la conception, des langages de requêtes et des usages des systèmes de gestion de bases de données.
> L'enjeu pour l'enseignant est de maîtriser avec le recul nécessaire ces thèmes du programme de terminale.
>
> Alignement sur le projet de programme NSI
>
> - Structures de données (terminale)
> - Langages et programmation (terminale)
> - Bases de données (terminale)

### Programme détaillé du bloc 4

Le programme vise l'acquisition _de compétences fondamentales_ dans les domaines suivants, complétés par une activité de synthèse FOAD. La partie bases de données est scindée en deux parties pour une meilleure digestion.

- [**Paradigmes de programmation**](1_paradigmes_de_programmation/)
  - CM introduction, impératif, fonctionnel, et objet
  - TD/TP application à la programmation fonctionnelle
- [**BD 1/2 : introduction et SQL**](3_bases_de_donnees_introduction/)
  - CM introduction, SGBD, SQL et SQLite
  - TD/TP SQL sélection
- [**BD 2/2 : création de schémas et normalisation**](4_bases_de_donnees_normalisation/)
  - CM redondances, DF, normalisation et conception
  - TD/TP création de schéma et normalisation
  - TD/TP programmation, algorithmes de jointures en Python
- [**Introduction aux structures de données**](2_structures_de_donnees/)
  - CM intro générale, liste chainée, pile et file
  - TD/TP liste/pile/file, en OOP

## Calendrier

Face à la situation sanitaire et les règles actuellement en cours à l'Université, le bloc privilégie _le travail asynchrone_ et la _minimisation du temps en classe virtuelle_ avec la diffusion des contenus et une assistance en ligne.
La semaine initialement prévue _en présentiel_ ou _face-à-face pédagogique_ est celle du 7 juin 2021, elle reste celle où auront lieu tous les temps d'échange synchrones en visio.

### Phase préparatoire (avant la semaine du 7 juin)

Nous vous demandons de consulter les vidéos des CM avant la semaine du 7 juin (cf. le readme dans chacun des 4 dossiers cités au dessus). Essayez également de lire les énoncés des TD/TP (les préparer serait mieux mais nous savons que vous avez peu de temps pour ça).

Les échanges asynchrones se font via un forum de discussion et le présent Git.

### Semaine du 7 juin

- Quelques mots d'introduction - Nicolas Pronost
  - [Lien visio](https://univ-lyon1.webex.com/meet/nicolas.pronost) - lundi 7 juin 8h45-9h
- [**Paradigmes de programmation**](1_paradigmes_de_programmation/) - Emmanuel Coquery
  - Séance de réponses aux questions sur les CM, débriefing et discussions - lundi 7 juin 9h-10h [lien visio](https://univ-lyon1.webex.com/meet/emmanuel.coquery) (! il est différent de celui de 8h45)
  - TD/TP application à la programmation fonctionnelle - lundi 7 juin 10h-12h et 14h-16h, sur le rocket-chat.
    En cas de besoin, une session visio sera ouverte.
- [**BD 1/2 : introduction et SQL**](3_bases_de_donnees_introduction/) - Fabien De Marchi - [lien visio](https://univ-lyon1.webex.com/univ-lyon1/j.php?MTID=m15867747e48770cf7232e098e469ba7e)
  - Séance de réponses aux questions sur les CM, débriefing et discussions - mardi 8 juin 9h-10h
  - TD/TP SQL sélection - mardi 8 juin 10h-12h et 14h-16h
- [**BD 2/2 : création de schémas et normalisation**](4_bases_de_donnees_normalisation/) - Fabien De Marchi - [lien visio](https://univ-lyon1.webex.com/univ-lyon1/j.php?MTID=m15867747e48770cf7232e098e469ba7e)
  - Séance de réponses aux questions sur les CM, débriefing et discussions - jeudi 10 juin 9h-10h
  - TD/TP création de schéma et normalisation - jeudi 10 juin 10h-12h
  - TD/TP programmation, algorithmes de jointures en Python - jeudi 10 juin 14h-16h
- [**Introduction aux structures de données**](2_structures_de_donnees/) - Nicolas Pronost - [lien visio pour la journée](https://univ-lyon1.webex.com/meet/nicolas.pronost)
  - Séance de réponses aux questions sur les CM, débriefing et discussions - vendredi 11 juin 9h-10h
  - TD/TP liste/pile/file, en OOP - vendredi 11 juin 10h-12h et 14h-16h

#### Remarques

- Les séances de réponses, débriefing et discussions sont des temps où, en plus du forum, un salon visio sera ouvert **pour répondre à vos questions sur chaque chapitre**, pour notamment les personnes qui n'ont pas eu les moyens d'avancer en amont de la semaine du 7 juin, et une remédiation et discussions des informations des CM seront possibles.
- Les séances de TD/TP sont des temps de travail sur des exercices, projets etc. où des éléments de correction seront aussi rendus disponibles et discutés.

### Modalités de contrôle des connaissances

L'évaluation portera sur deux épreuves :

- **Le vendredi 18 juin** : une évaluation individuelle en ligne (type QROC et QCM) portant sur tout le contenu du bloc. L'examen en ligne se fera en suivant ce [lien](https://clarolineconnect-exam.univ-lyon1.fr/resource/open/ujm_exercise/1059077) (utiliser votre compte Lyon 1 pour vous identifier). L'examen sera accessible le vendredi 18/06 de 8h00 à 20h00. Une fois commencé, vous avez 2h pour répondre aux questions.
- **Pour le 18 juin au plus tard** : un travail de production de contenus pédagogiques dit "Formation Ouverte A Distance" (FOAD). Les modalités sont détaillées dans [ce dossier](6_FOAD)

## Logiciels utilisés

### Pour la formation à distance

Nous utiliserons les outils suivants :

- [GitLab](https://forge.univ-lyon1.fr/diu-eil/bloc4), pour l'ensemble des informations et contenus pédagogiques
- [Rocket-chat](https://chat-info.univ-lyon1.fr), pour les discussions asynchrones
- [Webex](univ-lyon1.webex.com), pour la classe virtuelle synchrone
- YouTube, pour les cours non interactifs

Remarques :

- Tout est accessible en ligne avec un navigateur **à jour**.
- Des clients lourds (hors navigateur) existent également pour Git.
- Les clients Git peuvent également être intégrés à votre éditeur favori

### Pour les enseignements

- Pour la partie SQL, installer <https://sqlitebrowser.org/> (utilisable en classe de lycée)
- Pour la partie programmation : un environnement Python 3 et un IDE (au choix)

## Références

### Programmes NSI et DIU

- DIU Enseigner l’informatique au lycée <https://sourcesup.renater.fr/www/diu-eil/> dont [le programme V2](https://sourcesup.renater.fr/www/diu-eil/medias/diu-eil-habilit-2-ppn-revision-nov-2019.pdf)
- Programmes et ressources en numérique et sciences informatiques : <https://eduscol.education.fr/cid144156/nsi-bac-2021.html>
- Un ensemble de ressources par David Roche <https://pixees.fr/informatiquelycee/>

### Ressources autres DIU-EIL

- Paris 6 (Jussieu), blocs 1 et 4 <http://www-bd.lip6.fr/wiki/site/enseignement/diu/start>
- Paris 13, bloc 4 <https://lipn.univ-paris13.fr/~breuvart/DIU-EIL/>
- Lille <https://gitlab-fil.univ-lille.fr/diu-eil-lil/portail/blob/master/Readme.md>
- Antilles <https://ecursus.univ-antilles.fr/course/view.php?id=2389>
- Grenoble, blocs 1, 2 et 3 <https://diu-eil.gricad-pages.univ-grenoble-alpes.fr/>
- Strasbourg <https://moodle3.unistra.fr/course/view.php?id=1728>

### Ouvrages de référence

- Introduction à la science informatique <https://www.epi.asso.fr/revue/sites/s1112a.htm>
- Informatique et Sciences du Numériques (en python) <https://wiki.inria.fr/wikis/sciencinfolycee/images/a/a7/Informatique_et_Sciences_du_Num%C3%A9rique_-_Sp%C3%A9cialit%C3%A9_ISN_en_Terminale_S._version_Python.pdf>

### Programmation Python

Quelques références incontournables commentées

- Docs officielle <https://docs.python.org/3/index.html> dont tutoriaux <https://docs.python.org/3/tutorial/> : _la référence officielle_ un peu aride, mais reste la référence officielle. Les tutoriaux sont très accessibles et complètent bien la doc.
- [Practical Python Programming](https://dabeaz-course.github.io/practical-python/) ([Github](https://github.com/dabeaz-course/practical-python)) : **un bon cours assez court sur la pratique de Python**
- [Fluent Python](http://shop.oreilly.com/product/0636920032519.do), extraits de code sur <https://github.com/fluentpython> : **une mine d'or pour aller plus loin dans Python**
- The Hitchhiker’s Guide to Python! <https://docs.python-guide.org/> : **bon complément à la doc officielle, beaucoup plus orienté pratique**
- Problem Solving with Algorithms and Data Structures using Python <https://runestone.academy/runestone/books/published/pythonds/index.html#> : **libre et ouvert, très complet** avec de applications utilisables en classe

Autres références d'intérêt

- Real Python Tutorials <https://realpython.com/> : je ne suis pas très fan du modèle commercial, mais il y a des pas mal de posts gratuits (les vidéos sont payante) et certains contenus sont très bons voire exceptionnels (e.g., [Primer on Python Decorators](https://realpython.com/primer-on-python-decorators/) ou [Your Guide to the CPython Source Code](https://realpython.com/cpython-source-code-guide/))
- Awesome Python : A curated list of awesome Python frameworks, libraries, software and resources. <https://github.com/vinta/awesome-python> : _un peu en vrac, mais beaucoup de bibliothèques tierces de référence_ : utile si vous avez une tâche originale à réaliser et que vous cherchez des outils pour la résoudre
- Intermediate Python <https://book.pythontips.com> (sur [GitHub](https://github.com/yasoob/intermediatePython)) : _un peu dans l'esprit de Hitchhiker’s_
- The Algorithms - Python : All algorithms implemented in Python (for education) <https://github.com/TheAlgorithms/Python> : _les grands classiques en Python_ avec beaucoup d'application pour la classe
- Algorithms and data structures for preparing programming competitions: basic and advanced (en Python) <https://tryalgo.org> <https://github.com/jilljenn/tryalgo> : _orienté concours de programmation_ avec de nombreux alogorithmes implémentés, une bonne partie accessible au lycée.
