\documentclass[table]{beamer} %handout %hyperref={pdfpagelabels=false}
\usepackage[french]{babel}
\usepackage{tikz}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{beamerthemesplit}
\usetheme{AnnArbor}
\usecolortheme{beaver}
\usefonttheme{professionalfonts}

\usepackage{wasysym}
\usepackage{times}
\usepackage{subfigure}
\usepackage{fancyvrb}
\usepackage{booktabs,multirow,rotating, tabularx}
\usepackage{graphicx}

\newcommand{\vertc}[1]{\rotatebox{90}{\emph{#1}}}
\newcommand{\mc}[3]{\multicolumn{#1}{#2}{#3}}
\newcommand{\mr}[3]{\multirow{#1}{#2}{#3}}
	
\definecolor{gris10}{rgb}{0.75,0.75,0.75}

\usepackage{listings}
\usepackage{graphicx}
\lstdefinelanguage{SQLRT}[]{SQL}{
            morekeywords={ENUM,ENGINE,IF,REFERENCES,CHARSET,REPLACE},
            deletekeywords={ACTION}
    }[keywords,comments,strings]
%Syntaxe SQL modifiée

\lstset{language=SQLRT,tabsize=2,breaklines=true,basicstyle=\scriptsize,numbers=none,numberstyle=\footnotesize,stepnumber=2,frame=none,
        emph={Student,Apply,College},emphstyle=\underbar}

%------------------------------------------------------------------------------------

\hypersetup{
    pdfauthor   = {Romuald THION},%
    pdftitle    = {DIU-EIL Bloc 4},%
    pdfsubject  = {Le modèle relationnel},%
    pdfkeywords = {DIU, bases de données, algèbre relationnelle},%
    colorlinks  = false,
    linkbordercolor = blue, % hyperlink borders will be red
    pdfborderstyle = {/S/U/W 1}% border style will be underline of width 1pt
}	

\title[DIU-EIL Bloc 4 -- BD]{
    \textsc{DIU-EIL Bloc 4\\ Modèle relationnel et bases de données : normalisation et conception}
}

\author[R. THION]{
    Romuald THION
}
\institute{ 
{  
    \url{https://forge.univ-lyon1.fr/diu-eil/bloc4}}
}

%\date{Mardi 21 juin 2011 (matin)}

\newcommand{\bleu}[1]{{\color{cyan} #1}}
%-------------------------------------------------------------------------------

\usefoottemplate{%
  \vbox{% 
    \tinycolouredline{black}%
      {\color{white}\insertshortauthor\hfill\insertshorttitle\hfill\insertframenumber}%
}}

\setbeamertemplate{navigation symbols}{}
\beamertemplatetransparentcovereddynamicmedium 

\AtBeginSubsection[]{
  \begin{frame}%{Sommaire}
  \tableofcontents[currentsection, hideothersubsections]
  \end{frame} 
}

\AtBeginSection[]{
  \begin{frame}%{Sommaire}
  \tableofcontents[currentsection, hideothersubsections]
  \end{frame} 
}


\newcommand{\univers}{\cal U}
\newcommand{\valeurs}{\cal D}
\newcommand{\R}{\mathbf{R}}
\newcommand{\df}{\to}
\newcommand{\di}{\subseteq}


%-------------------------------------------------------------------------------
\begin{document}
	\begin{frame}
		\titlepage
	\end{frame}

    \begin{frame}{Plan}
        \tableofcontents[hideallsubsections]
    \end{frame} 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Problème et motivation : les anomalies}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Objectifs}
  \begin{alertblock}{Modéliser}
   Modéliser consiste à définir un monde abstrait qui coïncide avec les manifestations apparentes du monde réel.
  \begin{itemize}
   \item  il s'agit donc de déterminer l'ensemble des attributs, des relations et des contraintes qui constitueront le modèle. 
  \end{itemize}
  \end{alertblock}
  \alert{Nous allons voir :}
  \begin{itemize}
   \item  Quelles sont les propriété attendues d'une \alert{bonne} modélisation; 
   \item  Comment les obtenir. 
  \end{itemize}
\end{frame}


\begin{frame}{Positionnement dans le programme}
    \begin{figure}
      \centering
      \includegraphics[width=0.8\linewidth]{./programme_term1.png}%
    \end{figure}
  
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{Exemple}
   Soit ${\univers} = \{\mathtt{NumEt}, \mathtt{NomEt}, \mathtt{Adresse}, \mathtt{NumUE}, \mathtt{Titre}, \mathtt{Note}\}$ l'ensemble d'attributs (appelé \emph{univers}) décrivant des étudiants et des cours. Soient les deux schémas de BD suivants :
   
   \begin{itemize}
  \item $BD_1 = \{Donnees\}$ avec $schema(Donnees) = \univers$\\ \emph{une seule relation qui contient tout, comme dans un tableur}
  \item $BD_2 = \{Etudiant, UE, Inscrit\}$ avec
    \begin{itemize}
      \item $schema(Etudiant) = \{\mathtt{NumEt}, \mathtt{NomEt}, \mathtt{Adresse}\}$
      \item $schema(UE) = \{\mathtt{NumUE}, \mathtt{Titre}\}$
      \item $schema(Inscrit) = \{\mathtt{NumEt}, \mathtt{NumUE}, \mathtt{Note}\}$
    \end{itemize}
  \end{itemize}


\end{frame}

%%%%%%%%%%%%%

\begin{frame}{Exemple}
  \begin{block}{}
  \begin{table}
  \begin{center}
    \begin{tabular}{lcccccc}
    &\texttt{NumEt}& \texttt{NomEt}& \texttt{Adresse}& \texttt{NumUE}& \texttt{Titre}& \texttt{Note}\\
    \toprule
            & 124&   Jean  & Paris      & F234  & Philo I   &  A  \\
            & 456&   Emma  & Lyon      &  F234  & Philo I   &  B \\
            & 789&   Paul  & Marseille &  M321  & Analyse I  & C \\
            & 124&   Jean  & Paris     &  M321  & Analyse I  & A \\
            & 789&   Paul  & Marseille &  CS24  & BD I      &  B \\
    \bottomrule
    \end{tabular}
  \end{center}
  \caption{La relation universelle de tous les attributs de l'univers $\univers$}
  \end{table}
  \end{block}
  
  \begin{center}
    \alert{Comment évaluer ces deux schémas ? Lequel est meilleur ? Pourquoi ? Selon quels critères ?}
  \end{center}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{Anomalie de \emph{modification}}
  \begin{center}
    \begin{tabular}{lcccccc}
      &\texttt{NumEt}& \texttt{NomEt}& \texttt{Adresse}& \texttt{NumUE}& \texttt{Titre}& \texttt{Note}\\
    \toprule
    & {\color{red} 124}&   {\color{red} Jean} &  {\color{red}  Paris}  & {\color{blue}  F234} & {\color{blue} Philo I}&  A  \\
    & 456&   Emma & Lyon &  {\color{blue}F234} & {\color{blue} Philo I}&  B \\
     & {\color{orange}789}&   {\color{orange} Paul} & {\color{orange}Marseille}&  {\color{green} M321}&  {\color{green} Analyse I}  & C \\
     & {\color{red} 124}&   {\color{red} Jean} &  {\color{red} Paris}&  {\color{green} M321} &{\color{green}  Analyse I}  & A \\
     & {\color{orange}789}&   {\color{orange}Paul}& {\color{orange}Marseille} &   CS24&  BD I&  B\\
    \bottomrule
    \end{tabular}
  \end{center}

  \begin{alertblock}{Anomalie de \emph{modification}}
  \begin{center}
      \emph{Une modification sur \emph{une} ligne peut nécessiter des modifications sur \emph{d'autres} lignes.}
  \end{center}
  \end{alertblock}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{Anomalie de \emph{suppression}}
  \begin{center}
    \begin{tabular}{lcccccc}
      &\texttt{NumEt}& \texttt{NomEt}& \texttt{Adresse}& \texttt{NumUE}& \texttt{Titre}& \texttt{Note}\\
    \toprule
    & 124&   Jean &   Paris  & F234 & Philo I&  A  \\
    & 456&   Emma & Lyon &  F234 & Philo I&  B \\
     & 789&   Paul & Marseille&  M321&  Analyse I  & C \\
     & 124&   Jean &  Paris&  M321 & Analyse I  & A \\
     & 789&   Paul& Marseille &    \alert{CS24}&  \alert{BD I}&  B\\
     \bottomrule
    \end{tabular}
  \end{center}

  \begin{alertblock}{Anomalie de \emph{suppression}}
  \begin{center}
      \emph{Certaines informations dépendent de l'existence  \emph{d'autres informations}.}
  \end{center}
  \end{alertblock}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{Anomalie d'\emph{insertion}}
  \begin{center}
    \begin{tabular}{lcccccc}
      &\texttt{NumEt}& \texttt{NomEt}& \texttt{Adresse}& \texttt{NumUE}& \texttt{Titre}& \texttt{Note}\\
    \toprule
    & 124&   Jean &   Paris  & F234 & Philo I&  A  \\
    & 456&   Emma & Lyon &  F234 & Philo I&  B \\
     & 789&   Paul & Marseille&  M321&  Analyse I  & C \\
     & 124&   Jean &  Paris&  M321 & Analyse I  & A \\
     & 789&   Paul& Marseille &   CS24&  BD I&  B\\
     &145 &  Evariste  &  Aubenas& \alert{???}  & \alert{???}  & \alert{???} \\
      \bottomrule
    \end{tabular}
  \end{center}

  \begin{alertblock}{Anomalie d'\emph{insertion}}
  \begin{center}
      \emph{La possibilité d'enregistrer un tuple implique la connaissance de toutes les informations qui lui sont liées : problème de valeurs manquantes.}% (pas de valeurs nulles dans le cours).
  \end{center}
  \end{alertblock}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{Comment formaliser tout ça ?}
  \begin{center}
    \alert{Le moyen qui permet d'éviter ces problèmes est l'étude des dépendances et de la normalisation.}
  \end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{La théorie de la normalisation}\label{sec:theorie}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Les dépendances fonctionnelles}\label{sec:sub:dfs}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Définition}
  \begin{itemize}
   \item La forme la plus fréquemment rencontrée de dépendances.
  % \item Sont  à l'origine de l'approche par décomposition des schémas.
   \item Formalisent la notion de \alert{clef} (identifiant) d'une relation.
   \item Permettent de définir les \og{}bon\fg{} schémas (sans redondance)
  \end{itemize}

  \begin{alertblock}{Syntaxe des dépendances fonctionnelles}
    Une \emph{Dépendance Fonctionnelle (DF)} sur un schéma de relation $R \subseteq \univers$ est une expression de la forme 
    $$R:X\rightarrow Y\text{, avec }X,Y \subseteq R$$
    \begin{itemize}
     \item Une DF $X\rightarrow Y$ est dite \alert{triviale} si $Y\subseteq X$
     \item Une DF \alert{standard} si $X \neq \emptyset$.
    \end{itemize}
  \end{alertblock}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{\insertsubsection}
  \begin{block}{} %{Préliminaires}
    Soient $\univers$ un ensemble d'attributs et $\valeurs$ un domaine et $R\subseteq \univers$ :
    \begin{itemize}
      \item un tuple $t$ de $R$ est \alert{une fonction} $R \to \valeurs$
      \item une instance $r$ de $R$ est un \alert{ensemble fini de tuples}
      \item la \alert{projection} de $t$ sur $X \subseteq R$ notée\footnote{On écrit plutôt $t_{\mid X}$ en mathématiques usuelles} $t[X]$ est la \alert{restriction} de $t$ à $X$
    \end{itemize}
  \end{block}
  
  \begin{alertblock}{Sémantique des dépendances fonctionnelles}
  Soit $r$ une instance de relation sur $R$. 
    Une DF $R:X\df Y$ est \emph{satisfaite} dans $r$, noté $r \models X \df Y$, \emph{ssi} 
    $$\forall t_1,t_2 \in r. t_1[X]=t_2[X] \Rightarrow t_1[Y]=t_2[Y]$$
    
  On dit aussi que $X$ \emph{détermine (fonctionnellement)} $Y$ dans $r$.
  \end{alertblock}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{\insertsubsection}
  \begin{exampleblock}{Exemple}
  \begin{center}
    \begin{tabular}{lcccccc}
    \emph{r} &\texttt{NumEt}& \texttt{NomEt}& \texttt{Adresse}& \texttt{NumUE}& \texttt{Titre}& \texttt{Note}\\
    \toprule
            & 124&   Jean  & Paris      & F234  & Philo I   &  A  \\
            & 456&   Emma  & Lyon      &  F234  & Philo I   &  B \\
            & 789&   Paul  & Marseille &  M321  & Analyse I  & C \\
            & 124&   Jean  & Paris     &  M321  & Analyse I  & A \\
            & 789&   Paul  & Marseille &  CS24  & BD I      &  B \\
    \bottomrule
    \end{tabular}
  \end{center}

  \begin{itemize}
    \item $r \models \texttt{NumEt} \to \texttt{NomEt}$ et $r \models \texttt{NumEt}, \texttt{NumUE} \to \texttt{Note}$
    \item  $r \models \texttt{Adresse} \to \texttt{NumEt}$ \alert{($\dagger$)}
    \item  $r \not\models \texttt{NumEt} \to \texttt{NumUE}$ et $r \not\models\texttt{NumUE} \to \texttt{Note}$
  \end{itemize}
  \end{exampleblock}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{\insertsubsection}
  \begin{alertblock}{Clef}
     Une clef peut-être définie de deux manières \emph{équivalentes} :
    \begin{itemize}
    \item Une clef est un ensemble d'attributs $X$ \emph{qui ne prend jamais deux fois la même valeur} dans $r$
    \item Une clef est un ensemble d'attributs qui détermine tout $R$, c'est-à-dire tel que \alert{$r:$ $X \df R$}
    \end{itemize}
  \end{alertblock}

  \begin{alertblock}{Clef primaire (\emph{primary key})}
    \alert{Une clef primaire} est simplement \emph{une} clef parmi les autres (appelées \emph{clefs candidates}), choisie par le concepteur pour sa simplicité ou son aspect naturel.
  \end{alertblock}

\end{frame}

%%%%%%%%%%%%%




\subsection{Les formes normales}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{\insertsubsection}
  \begin{center}
    La solution aux problèmes des anomalies consiste à \alert{normaliser la relation} en la décomposant. Cette décomposition s'appuie sur les dépendances fonctionnelles qui existent entre les attributs.
  \end{center}

  \begin{alertblock}{Les formes normales permettent de spécifier formellement la notion \emph{intuitive} de \og{}bon schéma\fg{}}
  L'idée générale est de n'avoir \alert{que les clés} à vérifier et d'éliminer au maximum des DF qui ne définissent pas des clés.
  \end{alertblock}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{Inclusion des formes normales}
  \begin{figure}
    \centering
  \includegraphics[width=\textwidth]{./NormalForms.png}
  \caption{Pour les DFs, plusieurs Formes Normales (FN) de plus en plus restrictives (1FN, 2FN, 3FN, FN de Boyce-Codd -- FNBC). Au delà, d'autres types de dépendances sont nécessaires.}
  \end{figure}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{\insertsubsection}
  \begin{center}
    Soit $R$ un schéma de relation et $F$ un ensemble de DF définies sur $R$.
    Un schéma de BD est en $n$FN si tous ses schémas de relations le sont.
  \end{center}
    
  \begin{block}{Des contraintes de plus en plus restrictives}
     \begin{description}
       \item[1FN] toutes les valeurs des attributs sont \alert{atomiques}
       
       \item[2FN] $R$ est en 1FN et aucun attribut non clef ne dépend \alert{partiellement} d'une clef candidate.\\
       \emph{E.g., \alert{ $\{AB \df C, B \df C\}$} n'est \alert{pas} en 2FN.}

      \item[3FN] $R$ est en 2FN et toutes les DFs sont directes : tout attribut non clef dépend \alert{directement} d'une clé (sans transitivité).\\
      \emph{E.g., \alert{ $\{A \df B, B \df C\}$} est en 2FN mais \alert{pas} en 3FN.}
      
      \item[\alert{FNBC}] $R$ est en FN de Boyce-Codd ssi pour chaque DF $X\df A$ de $F$, $X$ contient une clef de $R$.\\
      \emph{E.g., \alert{ $\{AB \df C, C \df B\}$} est en 3FN mais \alert{pas} en FNBC.}
     \end{description}
  \end{block}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{La FNBC : une forme idéale}
  \begin{center}
    
    \alert{La FNBC est, \emph{pour les DF}, la forme idéale d'un schéma de BD}
    
    \og{}\emph{La clef, toute la clef et rien que la clef.}\fg{}
  \end{center}

  \begin{block}{Formalisation de la notion de redondance avec les DF}
     Ils existent $X \to A \in F$ et $t_i \neq t_j \in r$ t.q. $t_i[XA] = t_j[XA]$
  \end{block}


  \begin{alertblock}{Les trois propriétés suivantes sont équivalentes}
  \begin{itemize}
  \item $R$ est en FNBC par rapport à $F$
  \item $R$ n'a pas de problème de redondances par rapport à $F$
  \item ($R$ n'a pas de problème de  mise-à-jour par rapport à $F$)
  \end{itemize}
  \end{alertblock}
\end{frame}

%%%%%%%%%%%%%

\begin{frame}{Reprise de l'exemple}
  \begin{center}
    \begin{tabular}{lcccccc}
      &\texttt{NumEt}& \texttt{NomEt}& \texttt{Adresse}& \texttt{NumUE}& \texttt{Titre}& \texttt{Note}\\
    \toprule
    $t_1$& {\color{red} 124}&   {\color{red} Jean} &  {\color{red}  Paris}  & {\color{blue}  F234} & {\color{blue} Philo I}&  A  \\
    $t_2$& 456&   Emma & Lyon &  {\color{blue}F234} & {\color{blue} Philo I}&  B \\
    $t_3$& {\color{orange}789}&   {\color{orange} Paul} & {\color{orange}Marseille}&  {\color{green} M321}&  {\color{green} Analyse I}  & C \\
    $t_4$& {\color{red} 124}&   {\color{red} Jean} &  {\color{red} Paris}&  {\color{green} M321} &{\color{green}  Analyse I}  & A \\
    $t_5$& {\color{orange}789}&   {\color{orange}Paul}& {\color{orange}Marseille} &   CS24&  BD I&  B\\
    \bottomrule
    \end{tabular}
  \end{center}

  \begin{alertblock}{Vers un bon schéma}
  Ici, beaucoup de redondances, car $\texttt{NumEt} \to \texttt{NomEt}, \texttt{Adresse}$ et $\texttt{NumUE} \to \texttt{Titre}$ et $\texttt{NumEt}, \texttt{NumUE} \to \texttt{Note}$. 
  
  \begin{center}
    Mais on voit \og{}un bon schéma\fg{} commencer à apparaître !
  \end{center}
  \end{alertblock}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{La normalisation}\label{sec:normalisation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{frame}{Normaliser}
\begin{center}
    L'activité qui consiste, étant donné un ensemble de DF\footnote{On définit les DFs \emph{a priori}, à partir de la sémantique des données, on ne part pas d'une instance qui pourrait vérifier \og{}par hasard\fg{} $r \models \texttt{Adresse} \to \texttt{NumEt}$ !}, à avoir un schéma \emph{en bonne forme normale}.
\end{center}

\begin{itemize}
  \item Soit de façon \alert{calculatoire}, avec l'aide des DFs et d'algorithmes :
    \begin{itemize}
      \item \emph{Par synthèse} : on génère les schémas de relation à partir des DFs
      \item \emph{Par décomposition} : on raffine successivement $\univers$
    \end{itemize}
  \item Soit par \alert{construction}, avec les schéma Entités-Associations (E/A)
\end{itemize}


\begin{exampleblock}{Exemple filé de la base étudiant}
  %Avec $\texttt{NumEt} \to \texttt{NomEt}, \texttt{Adresse}$ et $\texttt{NumUE} \to \texttt{Titre}$ et $\texttt{NumEt}, \texttt{NumUE} \to \texttt{Note}$, 
  On obtient (par synthèse ou décomposition):
  \begin{itemize}
    \item $R_0(\mathtt{NumEt}, \mathtt{NomEt}, \mathtt{Adresse})$, i.e., \texttt{Etudiant} !
    \item $R_1(\mathtt{NumUE}, \mathtt{Titre})$, i.e., \texttt{UE} !
    \item $R_2(\mathtt{NumEt}, \mathtt{NumUE},  \mathtt{Note})$, i.e., \texttt{Inscrit} !
  \end{itemize}
\end{exampleblock}

\end{frame}


%%%%%%%%%%%%%

\begin{frame}{Quand ne \emph{pas} normaliser ?}
  La normalisation n'est \emph{pas} une obligation on peut vouloir s'en passer :
  \begin{itemize}
    \item Pour retrouver \og{}toutes\fg{} les données (originales), il faut calculer des  jointures, qui peuvent être \alert{coûteuses} :
    \begin{itemize}
      \item elle sont généralement nombreuses car la décomposition est maximale,
      \item leur calcul n'est pas toujours performant, en particulier si les index ne sont pas adaptés.
    \end{itemize}
    \item La normalisation peut être difficile et donc coûteuse. % en travail humain. % surtout pour obtenir des formes normales élevées
    \item On en a pas nécessairement besoin quand la base n'a pas une très grande durée de vie ou s'il n'y a jamais de modifications.
  \end{itemize}
\end{frame}







  \subsection{Les approches calculatoires}\label{sec:sub:calcul}
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Décomposition}
      Étant donnés un schéma de relation $R$ et un ensemble $F$ de DF, on va décomposer $R$ en $R_1$,  $R_2$, \ldots, $R_n$ tels que
    \begin{itemize}
      \item Cette décomposition soit \alert{sans perte d'information} : on peut retrouver tout instance $r$ de $R$ en combinant \emph{par jointure naturelle} les instances $r_i$ sur les $R_i$
      \item Cette décomposition soit \alert{sans perte de dépendances} : après, jointure des $r_i$, on peut retrouver toutes les dépendances impliquées par $F$ à partir de leurs projections $F_i$ sur les $R_i$
      \item $R_1$,  $R_2$, \ldots, $R_n$ soient dans une forme normale maximale
    \end{itemize}

%  \begin{block}{Remarques}
%    \begin{itemize}
%    \item La 3FN est \emph{toujours} possible, quelque soit les DF considérées.
%    \item En revanche la FNBC n'est \emph{pas} {toujours} possible. :
%%      \begin{itemize}
%%        \item On peut bien obtenir une décomposition \alert{sans perte d'information}
%%        \item Mais pas toujours \alert{sans perte de dépendances}
%%      \end{itemize}
%    \end{itemize}
%  \end{block}



\begin{block}{Algorithme de décomposition et synthèse}
   \begin{itemize}
   \item $R = \univers$  la relation (universelle) à décomposer
   \item $F$ un \emph{ensemble \alert{minimal}} de dépendances  sur $R$
 \end{itemize}
\end{block}

  
\end{frame}


\begin{frame}{Algorithme de décomposition}
   \begin{block}{Algorithme de décomposition en FNBC}
  Soit $S = \{R\}$. Tant qu'il existe dans $R_i \in S$ qui n'est pas en FNBC :
    \begin{itemize}
  \item On cherche une dépendance non triviale $X \to Y$ telle que $R_i(X, Y, Z)$ et $X$ n'est pas une clé de $R_i$.
  \item On ajoute à $Y$ l'ensemble $Z'$  des attributs de $Z$ fonctionnellement déterminés par $X$, produisant la dépendance $X \to Y \cup Z'$.
  \item On remplace $R_i$ dans $S$ par les deux relations
  \begin{itemize}
    \item $R_1(X, Y \cup Z')$
    \item $R_2(X, Z \setminus Z')$
  \end{itemize}
  \end{itemize}

  \end{block}
   
  \begin{center}
    \alert{Propriété : cet algorithme est sans perte d'information mais pas toujours sans perte de dépendances}
  \end{center}
    
\end{frame}

%%%%%%%%%%%%%%%%

\begin{frame}{Algorithme de synthèse}

  \begin{block}{Algorithme de synthèse en 3FN/FNBC}
  \begin{itemize}
%  \item \alert{Construire une couverture minimale de $F$.}
  \item Générer une relation $XY$ pour chaque DF $X\df Y$;
%  \item Générer une relation $XY'$ pour chaque DMV $X \mvd Y$ avec $F\models Y'\df Y$;
  \item On supprime les schémas de relation qui ne sont pas maximaux par inclusion.
  \item S'il y a perte de jointure, alors on rajoute une relation composée d'une clé de $F$.
  \end{itemize}
  \end{block}
  
  \begin{center}
  \alert{Propriété : l'algorithme est sans perte d'information et  donne un schéma en 3FN ou en FNBC quand c'est possible sans perte de dépendance.}  
  \end{center}

\end{frame}


  \subsection{Les diagramme Entités-Associations}\label{sec:sub:EA}
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{\insertsubsection}
  \begin{center}
      Une autre façon d'avoir des schémas en FNBC, c'est d'avoir une méthode qui va l'assurer  \alert{par construction}, qui rend impossible la définition de schémas non normalisés. 
  \end{center}

  \begin{block}{Les diagrammes Entités-Associations (E/A)}
      \begin{figure}
        \centering
        \includegraphics[width=0.95\linewidth]{./base-etudiant-EA.png}%
      \end{figure}
    
  \end{block}

  \begin{center}
    Voir par exemple \url{https://perso.liris.cnrs.fr/fabien.duchateau/BDW1/}
  \end{center}
\end{frame}


\begin{frame}{\insertsubsection}
  \begin{block}{Méthode de conception avec E/A}
    \begin{enumerate}
      \item On définit d'abord un Modèle Conceptuel de Données (MCD) dans le formalisme E/A
      \begin{itemize}
        \item On identifie les \alert{entités} qui composent la base
        \item On identifie les \alert{associations} entre les entités, en précisant
          \begin{itemize}
            \item les \alert{cardinalités} de l'association
            \item les attributs de l'association 
          \end{itemize}
      \end{itemize}
      \item  On génère le schéma logique SQL à partir du MCD
       \begin{itemize}
         \item Pour chaque entité on crée une table
         \item Pour chaque association, on étudie les cardinalités et selon
         \begin{itemize}
           \item Cas \emph{many-to-many} on génère une nouvelle table 
           \item Cas \emph{many-to-one} on pousse l'association du côté du \emph{one}
         \end{itemize}
       \end{itemize}
    \end{enumerate}
  \end{block}


  \begin{center}
    Voir par exemple JMerise \url{http://www.jfreesoft.com/JMerise/}
  \end{center}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Les contraintes d'intégrité en SQL}\label{sec:SQL}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{De la normalisation aux contraintes d'intégrités SQL}
%  \begin{block}{}
%    \begin{itemize}
%      \item Une \emph{anomalie} a lieu lorsqu'\emph{à la suite d'une modification de la base, des contraintes sémantiques valides se trouvent violées}.
%      \item Des mécanismes de contrôle sont intégrés aux SGBDR pour éviter ce genre de problèmes
%    \end{itemize}
%  \end{block}

\begin{center}
    La normalisation permet d'obtenir des \og{}bons schémas\fg{} (en FNBC) sans anomalies. Comment implanter cela en SQL ?
\end{center}

  \begin{alertblock}{Principe}
    \begin{itemize}
      \item On fait l'hypothèse suivante \alert{le concepteur de BD n'implémente \emph{que} les clefs} (et les clés étrangères).
      \item Le contrôle automatique de ces contraintes par le SGBD est efficace et leur implémentation est toujours intégrée.
      \item Ainsi, toute mise-à-jour \emph{respecte les clés}.
    \end{itemize}
    \begin{center}
      Par contre, on ne peut pas aussi facilement et aussi efficacement garantir les DFs qui ne sont pas des clefs !
    \end{center}
  \end{alertblock}
\end{frame}


\begin{frame}{\insertsection}
  Le language SQL permet de spécifier lors de la définition des schémas des contraintes sur les instances autorisées
  
  \begin{alertblock}{Principales contraintes d'intégrité}
    \begin{itemize}
      \item \texttt{NOT NULL} : pas de valeur \texttt{NULL} pour l'attribut
      \item \texttt{UNIQUE} : les valeurs (différentes de \texttt{NULL}) doivent être toutes différentes
        \begin{itemize}
          \item Cette contrainte génère automatiquement \alert{un index} pour assurer efficacement la vérification de l'unicité
        \end{itemize}
      \item \texttt{PRIMARY KEY} :  la même chose que \texttt{UNIQUE} et \texttt{NOT NULL}
      \item \texttt{REFERENCES} : clef étrangère, les valeurs prises \emph{doivent} être présentes dans une colonne \texttt{UNIQUE} d'une autre table
    \end{itemize}
  \end{alertblock}
  
  \begin{center}
    \alert{Les contraintes \texttt{UNIQUE} (\texttt{NOT NULL}) permette d'imposer les clefs dans les tables.}
  \end{center}
\end{frame}

\begin{frame}{\insertsection}
\begin{exampleblock}{La définition de la base d'exemple}
  {
    \lstinputlisting[language=SQL]{base-etudiant.sql}}
\end{exampleblock}

\end{frame}

\end{document}
