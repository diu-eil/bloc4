CREATE TABLE Etudiant (
       NumEt INTEGER PRIMARY KEY,
       -- Traduit la DF NumEt -> NomEt, Adresse
       NomEt TEXT,
       Adresse TEXT
);
CREATE TABLE UE (
       NumUE INTEGER PRIMARY KEY,
       -- Traduit la DF NumUE -> Titre
       Titre TEXT
);
CREATE TABLE Inscrit (
       NumEt INTEGER REFERENCES Etudiant(NumEt),
       NumUE INTEGER REFERENCES UE(NumUE),
       Note REAL NOT NULL,
       PRIMARY KEY (NumEt, NumUE)
       -- Traduit la DF NumEt, NumUE -> Note
);

