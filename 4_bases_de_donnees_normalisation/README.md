DIU bloc 4 : "Bases de données : création de schémas et normalisation"
====================================================

Objectifs pédagogiques
----------------------

Extrait du programme du DIU :

> Structuration de l'information
> * Notion informelle de SGBD (Système de Gestion de Bases de Données)
> * Bases de données relationnelles, modèle relationnel, schémas, tables et relations
> * Contraintes des bases de données relationnelles : clés primaires, étrangères
>
> Langages de données et d'interrogation
> * Langage de requête : SQL
> * Programmation web côté serveur et interrogation d'une base de données

Contenus/programmes
-------------------

### Le cours 

Anomalies, dépendances fonctionnelles, normalisation, conception (dont E/A)

* [planches](conception_bd.pdf)
* La [vidéo YT de la partie 1](https://youtu.be/2ZnHB--Joo0) (anomalies, dépendances fonctionnnelles,)
* La [vidéo YT de la partie 2](https://youtu.be/nj8cUY16fAs) (normalisation, conception)

### Le TD/TP

Nous ferons les deux énoncés suivants ensembles en séance le jeudi 10 juin.

* [TP sur les liens entre E/A et relationnel](./ExerciceConceptionBatiments.pdf)
* [Implémentation des schémas et contraintes](TP/TP_conception_de_BD.pdf)


### Séances du jeudi 10 juin

 * Les séances seront [en visio WEBEX](
    https://univ-lyon1.webex.com/univ-lyon1/j.php?MTID=m15867747e48770cf7232e098e469ba7e)(Cliquez pour rejoindre la réunion)

### Références
----------

* Extraits du cours de licence troisième année en bases de données :
  * [Présentation des BD](https://perso.liris.cnrs.fr/fabien.demarchi/Cours_BD/Cours/Intro.pdf)
  * [Modèle EA](https://perso.liris.cnrs.fr/fabien.demarchi/Cours_BD/Cours/EA.pdf)
  * [Bases de données bien formées](https://perso.liris.cnrs.fr/fabien.demarchi/Cours_BD/Cours/FormesNormales.pdf)