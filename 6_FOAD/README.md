DIU bloc 4 : "Formation Ouverte A Distance" (FOAD)
==================================================

L'objectif de la FOAD est, en binôme, de pratiquer et approfondir les contenus du bloc en produisant _du matériel pédagogique utile en classe_ pour la spécialité NSI de Terminale, que ce soit des activités en classe ou de type projet. Ce travail doit être réalisé en 10h par participant, donc 20h pour le binôme, et la quantité à produire doit être équivalente à 3h de travail élève.

Les binômes s'inscrivent sur ce pad [(lien)](https://semestriel.framapad.org/p/themes-foad-bloc-4-9nqw?lang=fr) au plus tard le vendredi 11 juin midi. Le pad ne contient initialement que des thèmes d'exemples, vous êtes libre d'en ajouter.

Le rendu _pourra_ être accompagné d'une courte vidéo de présentation.
La date limite pour le rendu est fixée **au 18 juin 2021**.
Le dépôt se fait dans la case 'DépotFOAD' de l'UE INF0004E sur [Tomuss](https://tomuss.univ-lyon1.fr/). Un seul dépôt par l'un des membres du binôme suffit.

Modèle de description du travail FOAD 
-------------------------------------

A rendre sous ce format ou un autre lors de la remise de l'archive du travail FOAD.
Les informations demandées sont les suivantes :

* Noms et prénoms de chaque membre du binôme
* Intitulé du projet
* Résumé du projet (5 lignes max)
* Liste des supports proposés, avec pour chaque production
    * type de support (cours, exercices, TP, projet, examen)
    * durée pour les élèves
    * pré-requis des élèves

Critères d'évaluation indicatifs
--------------------------------

* 20% présentation, structure générale
* 20% originalité
* 20% ambition, périmètre fonctionnel de la réalisation
* 40% qualité technique

