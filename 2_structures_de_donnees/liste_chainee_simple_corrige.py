
class Cellule:
    """Type de base pour les noeuds de la liste"""
    def __init__(self, info_element, suivant):
        self.info = info_element
        self.suivant = suivant


class Liste:
    """Liste simplement chaînée non circulaire."""

    def __init__(self):
        self.premier = None

    def est_vide(self):
        """"Renvoie True si et seulement si la liste est vide"""
        return self.premier == None

    def vider(self):
        """"Supprime tous les éléments de la liste. Ne renvoie rien."""
        while not self.est_vide():
            self.supprimer_tete()

    def nb_elements(self):
        """"Retourne le nombre d'éléments de la liste"""
        nb = 0
        elt = self.premier
        while elt:
            nb += 1
            elt = elt.suivant
        return nb

    def ieme_element(self, indice):
        """"Retourne le i-ème élément de la liste
            Affiche une erreur ou lève une exception en cas d'indice trop grand"""
        if self.est_vide() :
            print("Erreur : liste vide")
            return None           
        elt = self.premier
        for i in range(indice):
            elt = elt.suivant
            if elt == None:
                print("Erreur ieme_element : indice trop grand")
                return None
        return elt.info

    def modifier_ieme_element(self, indice, info_element):
        """"Modifie le i-ème élément de la liste. Ne renvoie rien.
            Affiche une erreur ou lève une exception en cas d'indice trop grand"""
        if self.est_vide() :
            print("Erreur : liste vide")
        else :
            elt = self.premier
            for i in range(indice):
                elt = elt.suivant
                if elt == None:
                    print("Erreur modifier_ieme_element : indice trop grand")
                    return
            elt.info = info_element

    def afficher(self):
        """"Affiche tous les éléments de la liste de gauche à droite avec print(). Ne renvoie rien."""
        elt = self.premier
        while elt:
            print(elt.info, end=' ')
            elt = elt.suivant
        print()

    def ajouter_en_tete(self, info_element):
        """"Ajoute un premier élément (en tête de liste). Ne renvoie rien."""
        self.premier = Cellule(info_element, self.premier)

    def supprimer_tete(self):
        """"Supprime le premier élément (en tête de liste). Ne renvoie rien."""
        elt = self.premier
        self.premier = elt.suivant
        del elt

    def ajouter_en_queue(self, info_element):
        """"Ajoute un dernier élément (en queue de liste). Ne renvoie rien."""
        if self.est_vide():
            self.ajouter_en_tete(info_element)
        else:
            elt = self.premier
            while elt.suivant:
                elt = elt.suivant
            elt.suivant = Cellule(info_element, None)

    def rechercher_element(self, info_element):
        """"Renvoie l'indice de l'élément recherché et -1 si l'élément n'existe pas."""
        elt = self.premier
        trouve = False
        pos = 0
        while elt and not trouve:
            if info_element == elt.info:
                trouve = True
            else:
                elt = elt.suivant
                pos += 1
        if trouve:
            return pos
        else:
            return -1

    def inserer_element(self, indice, info_element):
        """"Insère un élément en i-ème position. Ne renvoie rien."""
        if self.est_vide() or indice == 0:
            self.ajouter_en_tete(info_element)
        elif indice == self.nb_elements():
            self.ajouter_en_queue(info_element)
        else:
            elt = self.premier
            for _ in range(indice-1): elt = elt.suivant
            elt.suivant = Cellule(info_element, elt.suivant)

    def importer_tableau(self, tab):
        self.vider()
        for elt in tab:
            self.ajouter_en_queue(elt)

# # ========================== programme principal =====================================
print("Creation de la liste")
ma_liste = Liste()

print("Liste vide ?", end=' ')
print(ma_liste.est_vide())

print("Ajouts en tete de 5, '2', 4, 6 et 1")
ma_liste.ajouter_en_tete(5)
ma_liste.ajouter_en_tete('2')
ma_liste.ajouter_en_tete(4)
ma_liste.ajouter_en_tete(6)
ma_liste.ajouter_en_tete(1)

print("Liste vide ?", end=' ')
print(ma_liste.est_vide())

print("Affichage de la liste : ", end='')
ma_liste.afficher()

print("La valeur de l'element a l'indice 1 est", ma_liste.ieme_element(1))

print("Modification de l'element a l'indice 1 (1.6) : ", end='')
ma_liste.modifier_ieme_element(1, 1.6)
ma_liste.afficher()

print("Le nombre d'elements actuels est", ma_liste.nb_elements())

print("Suppression de l'element de tete : ", end='')
ma_liste.supprimer_tete()
ma_liste.afficher()

print("Ajout en queue de 7 et 'test' : ", end='')
ma_liste.ajouter_en_queue(7)
ma_liste.ajouter_en_queue("test")
ma_liste.afficher()

print("Recherche de la valeur 4 :", ma_liste.rechercher_element(4))
print("Recherche de la valeur 'coucou' :", ma_liste.rechercher_element("coucou"))

print("Insertion de la valeur 10 a l'indice 3 : ", end='')
ma_liste.inserer_element(3, 10)
ma_liste.afficher()

print("Après suppression de tous les elements : ", end='')
ma_liste.vider()
ma_liste.afficher()

tab = [5,4,'a',-1]
print("Creation d'une liste a partir d'un tableau contenant [5, 4, 'a', -1] : ", end='')
ma_liste.importer_tableau(tab)
ma_liste.afficher()
