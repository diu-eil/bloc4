from collections.abc import Collection, Reversible, Iterable, Iterator

# =======================    Cellule   ===========================================
class Cellule:
    """ Element d'une liste """
    def __init__(self, info, precedent=None, suivant=None):
        self.info = info
        self.precedent = precedent
        self.suivant = suivant

# =========================    Liste   ===========================================
class Liste (Collection, Reversible):
    """ Liste doublement chaînée non circulaire """

    def __init__(self, iterable=()):
        """ Création de la Liste, vide par défaut """

    def __len__(self):
        """ Retourne le nombre d'éléments de la liste """

    def empty(self):
        """ Retourne si la liste est vide ou non """

    def append(self, infoElement):
        """ Ajoute l'élément en fin de liste (ajouterEnQueue) """

    def appendleft(self, infoElement):
        """ Ajoute l'élément en début de liste (ajouterEnTete) """

    def pop(self):
        """ Supprime et retourne l'élément en fin de liste """

    def popleft(self):
        """ Supprime et retourne l'élément en début de liste """

    def clear(self):
        """ Vide la liste """

    def __iter__(self):
        """ Retourne un itérateur sur la liste """
        # on créé un nouvel objet ListeIterateur qui commence sur la première cellule

    def __reversed__(self):
        """ Retourne un itérateur sur la liste inversée """
        # on créé un nouvel objet ListeIterateurInverse qui commence sur la dernière cellule

    def __contains__(self, infoElement):
        """ Indique si infoElement est dans la liste """

    def __repr__(self):
        """ Retourne la liste en format texte : Liste((élément1, élément2, ...)) """

    def __eq__(self, l):
        """ Teste si la liste est égale (mêmes éléments dans le même ordre) à l """

# =======================    Itérateurs   ===========================================
class ListeIterateur:
    """ Un itérateur sur Liste """

    def __init__(self, commencement):
        """ Initialise l'itérateur à la cellule de commencement """

    def __next__(self):
        """ Retourne la valeur de l'itérateur et passe à la cellule suivante """

    def __iter__(self):
        """ Retourne l'itérateur """


class ListeIterateurInverse:
    """ Un itérateur inverse sur Liste """

    def __init__(self, commencement):
        """ Initialise l'itérateur à la cellule de commencement """

    def __next__(self):
        """ Retourne la valeur de l'itérateur et passe à la cellule précédente """

    def __iter__(self):
        """ Retourne l'itérateur """

# =============================    Tests   ===========================================
# Test création de liste
liste = Liste()
assert not liste
assert hasattr(liste, "premier")
assert hasattr(liste, "dernier")
assert len(liste) == 0
assert liste.premier is None
assert liste.dernier is None
assert hasattr(liste, "taille")

# Test empty
assert liste.empty()

# Test chainage append
apres_append = liste.append(0)
assert id(liste) == id(apres_append)

# Test 1 append
assert len(liste) == 1
assert liste.premier == liste.dernier
assert liste.premier is not None

# Test 2 append
liste.append(1)
assert len(liste) == 2
assert liste.premier.suivant == liste.dernier
assert liste.dernier.precedent == liste.premier

# Test pop
valeur = liste.pop()
assert valeur == 1
valeur2 = liste.pop()
assert valeur2 == 0
assert liste.premier is None
assert liste.dernier is None
assert len(liste) == 0
assert not liste

# Test iterable
assert isinstance(liste, Iterable)
iterateur = iter(liste)
assert isinstance(iterateur, Iterable)
assert isinstance(iterateur, Iterator)

# Test idempotent
assert id(iterateur) == id(iter(iterateur))

# Test iteration
liste.append(0).append(1).append(2)
tab = list(liste)
assert tab == [0, 1, 2]

# Test iteration inverse
tabInverse = list(reversed(liste))
assert tabInverse == [2, 1, 0]

# Test égalité
liste2 = Liste()
liste2.append(0).append(1).append(2)
assert id(liste2) != id(liste)
assert liste2 == liste

# Test non égal
liste3 = Liste()
liste3.append(1).append(2).append(0)
assert id(liste3) != id(liste)
assert liste3 != liste

# Test repr
assert str(liste) == "Liste((0, 1, 2))"

# Test repr eval
assert eval(str(liste)) == liste

# Test création depuis iterable
liste4 = Liste([0, 1, 2])
assert list(liste4) == [0, 1, 2]

# Test clear
liste.clear()
assert not liste

# Test contains
liste.append(0).append(1).append(2)
assert 0 in liste
assert 1 in liste
assert 2 in liste
assert 42 not in liste
