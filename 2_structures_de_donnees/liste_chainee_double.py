"""Module pour les listes doublement chainées en POO
   Ce fichier est le squelette de départ du TD <01-_Liste_chaînée_-_TD.pdf>
   Il reprend les noms des méthodes du sujet (en `snake_case`).
   Pour une version plus "pyhtonic", voir dans un second temps le TP
   dans le dossier <TP_listes_pythoneuse>.
"""

class Cellule:
    """Type de base pour les noeuds de la liste"""
    def __init__(self, info_element, suivant, precedent):
        self.info = info_element
        self.suivant = suivant
        self.precedent = precedent


class Liste:
    """Liste doublement chaînée non circulaire."""

    def __init__(self):
        self.premier = None
        self.dernier = None

    def est_vide(self):
        """"Renvoie True si et seulement si la liste est vide"""
        raise NotImplementedError("TODO: implémenter cette méthode")

    def vider(self):
        """"Supprime tous les éléments de la liste. Ne renvoie rien."""
        raise NotImplementedError("TODO: implémenter cette méthode")

    def nb_elements(self):
        """"Retourne le nombre d'éléments de la liste"""
        raise NotImplementedError("TODO: implémenter cette méthode")

    def ieme_element(self, indice):
        """"Retourne le i-ème élément de la liste
            Affiche une erreur ou lève une exception en cas d'indice trop grand"""
        raise NotImplementedError("TODO: implémenter cette méthode")

    def modifier_ieme_element(self, indice, info_element):
        """"Modifie le i-ème élément de la liste. Ne renvoie rien.
            Affiche une erreur ou lève une exception en cas d'indice trop grand"""
        raise NotImplementedError("TODO: implémenter cette méthode")

    def afficher(self):
        """"Affiche tous les éléments de la liste de gauche à droite avec print(). Ne renvoie rien."""
        raise NotImplementedError("TODO: implémenter cette méthode")

    def ajouter_en_tete(self, info_element):
        """"Ajoute un premier élément (en tête de liste). Ne renvoie rien."""
        raise NotImplementedError("TODO: implémenter cette méthode")

    def supprimer_tete(self):
        """"Supprime le premier élément (en tête de liste). Ne renvoie rien."""
        raise NotImplementedError("TODO: implémenter cette méthode")

    def ajouter_en_queue(self, info_element):
        """"Ajoute un dernier élément (en queue de liste). Ne renvoie rien."""
        raise NotImplementedError("TODO: implémenter cette méthode")

    def rechercher_element(self, info_element):
        """"Renvoie l'indice de l'élément recherché et -1 si l'élément n'existe pas."""
        raise NotImplementedError("TODO: implémenter cette méthode")

    def inserer_element(self, indice, info_element):
        """"Insère un élément en i-ème position. Ne renvoie rien."""
        raise NotImplementedError("TODO: implémenter cette méthode")
