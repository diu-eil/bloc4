DIU bloc 4 : "Structures de données"
==========================================

### Contenus/programmes
  * Notion de structure de données abstraite
  * Listes, piles, files
  * Arbres binaires, arbres binaires de recherche

NB : Arbres et graphes seront vus dans le bloc 5.

### Introduction générale : panorama des structures de données

  * CM : [pdf](intro_structures.pdf) - [Vidéo Youtube](https://youtu.be/SzEj6cmbiNM)

### Listes (simplement et doublement) chaînées

  * CM : [pdf](01-_Liste_chaînée_-_CM.pdf) - [Vidéo Youtube](https://youtu.be/NqTpwyDsKWo)
  * TD : [énoncé](01-_Liste_chaînée_-_TD.pdf)
  * TP : Liste simplement chaînée : [squelette à compléter](liste_chainee_simple.py)
  * TP : Liste doublement chaînée : [squelette à compléter](liste_chainee_double.py)
  * TP : Liste doublement chaînée version "Pythonic" : [énoncé](TP_listes_pythoneuse/TP_listes_pythonic.md) - [squelette à compléter](TP_listes_pythoneuse/linked_list.py)

### Piles et files :
* CM : [pdf](02-_Pile_et_File_-_CM.pdf) - [Vidéo Youtube](https://youtu.be/hF3DTIbg-28)
* TD : [énoncé](02-_Pile_et_File_-_TD.pdf)

### Correction
* Liste simplement chaînée : [fichier python complet](liste_chainee_simple_corrige.py)
* Liste doublement chaînée : [fichier python complet](liste_chainee_double_corrige.py)
* Liste doublement chaînée Pythoneuse : [fichier python complet](linked_list_corrige.py)
* Pile et file : [fichier python complet](02-_Pile_et_File_-_TD.py)

Références
----------

* <https://www.bigocheatsheet.com/> (voir [pdf](big-o-cheatsheet.pdf))
* <https://en.wikipedia.org/wiki/List_of_data_structures>
* [Mémento Python](memento-python.pdf)
