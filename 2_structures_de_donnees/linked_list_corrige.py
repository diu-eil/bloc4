from collections.abc import Collection, Reversible, Iterable, Iterator

# =======================    Cellule   ===========================================
class Cellule:
    """ Element d'une liste """
    def __init__(self, info, precedent=None, suivant=None):
        self.info = info
        self.precedent = precedent
        self.suivant = suivant

# =========================    Liste   ===========================================
class Liste (Collection, Reversible):
    """ Liste doublement chaînée non circulaire """

    def __init__(self, iterable=()):
        """ Création de la Liste, vide par défaut """
        if not isinstance(iterable, Iterable):
            raise TypeError("Parametre iterable n'est pas Iterable")

        self.premier = None # pointeur sur la première cellule
        self.dernier = None # pointeur sur la dernière cellule
        self.taille = 0 # nombre d'éléments

        for element in iterable: # on ajoute les éléments de iterable (s'il y en a)
            self.append(element)

    def __len__(self):
        """ Retourne le nombre d'éléments de la liste """
        return self.taille

    def empty(self):
        """ Retourne si la liste est vide ou non """
        return len(self) == 0

    def append(self, infoElement):
        """ Ajoute l'élément en fin de liste (ajouterEnQueue) """
        if self.empty(): # premier ajout
            self.premier = self.dernier = Cellule(infoElement)
        else:
            # ajout d'un nouveau dernier
            self.dernier = Cellule(infoElement, self.dernier)
            # mise à jour de l'avant dernier
            self.dernier.precedent.suivant = self.dernier
        # mise à jour de la taille
        self.taille += 1
        # pour pouvoir enchaîner les appels (ex. l.append(0).append(1))
        return self

    def appendleft(self, infoElement):
        """ Ajoute l'élément en début de liste (ajouterEnTete) """
        if self.empty(): # premier ajout
            self.premier = self.dernier = Cellule(infoElement)
        else:
            # ajout d'un nouveau premier
            self.premier = Cellule(infoElement, None, self.premier)
            # mise à jour du deuxième
            self.premier.suivant.precedent = self.premier
        # mise à jour de la taille
        self.taille += 1
        # pour pouvoir enchaîner les appels (ex. l.appendleft(0).appendleft(1))
        return self

    def pop(self):
        """ Supprime et retourne l'élément en fin de liste """
        if self.empty() :
            raise IndexError("La liste est déjà vide")

        # on garde la dernière valeur pour la renvoyer à la fin
        valeur = self.dernier.info

        # on décale vers la gauche le dernier élément
        self.dernier = self.dernier.precedent
        self.taille -= 1

        # L'appel à del n'est pas obligatoire car la cellule sera collectée par le ramasse-miette
        # mais c'est aussi clair et ça libère au plus tôt
        if self.empty():
            # on avait une liste de taille 1 qui passe à 0
            # on supprime donc l'unique premier et dernier élément
            del self.premier
            self.premier = None
        else:
            # sinon, on ne supprime que le dernier
            del self.dernier.suivant
            self.dernier.suivant = None

        # on retourne la sauvegarde de l'info de l'élément supprimé
        return valeur

    def popleft(self):
        """ Supprime et retourne l'élément en début de liste """
        if self.empty() :
            raise IndexError("La liste est déjà vide")

        # on garde la première valeur pour la renvoyer à la fin
        valeur = self.premier.info

        # on décale vers la droite le premier élément
        self.premier = self.premier.suivant
        self.taille -= 1

        # L'appel à del n'est pas obligatoire car la cellule sera collectée par le ramasse-miette
        # mais c'est aussi clair et ça libère au plus tôt
        if self.empty():
            # on avait une liste de taille 1 qui passe à 0
            # on supprime donc l'unique premier et dernier élément
            del self.dernier
            self.dernier = None
        else:
            # sinon, on ne supprime que le premier
            del self.premier.precedent
            self.premier.precedent = None

        # on retourne la sauvegarde de l'info de l'élément supprimé
        return valeur

    def clear(self):
        """ Vide la liste """
        # on supprime le dernier élément tant que la liste n'est pas vide
        while not self.empty():
            self.pop()

    def __iter__(self):
        """ Retourne un itérateur sur la liste """
        # on créé un nouvel objet ListeIterateur qui commence sur la première cellule
        return ListeIterateur(self.premier)

    def __reversed__(self):
        """ Retourne un itérateur sur la liste inversée """
        # on créé un nouvel objet ListeIterateurInverse qui commence sur la dernière cellule
        return ListeIterateurInverse(self.dernier)

    def __contains__(self, infoElement):
        """ Indique si infoElement est dans la liste """
        # itération sur les éléments et test si égalité
        for element in self: # utilise les itérateurs (=> on obtient des info et non des cellules)
            if element == infoElement:
                return True
        return False

    def __repr__(self):
        """ Retourne la liste en format texte : Liste((élément1, élément2, ...)) """
        # on récupère le nom de la classe
        class_name = type(self).__name__
        # on écrit les éléments à partir de la liste sous forme d'un tuple
        return f'{class_name}({tuple(self)})'

    def __eq__(self, l):
        """ Teste si la liste est égale (mêmes éléments dans le même ordre) à l """
        if not isinstance(l, type(self)):
            return TypeError("Parametre l n'est pas du bon type")
        # S'appuie sur le protocole d'itération ET l'égalité sur le type list de Python
        return list(self) == list(l)

# =======================    Itérateurs   ===========================================
class ListeIterateur:
    """ Un itérateur sur Liste """

    def __init__(self, commencement):
        """ Initialise l'itérateur à la cellule de commencement """
        self.iterateur = commencement

    def __next__(self):
        """ Retourne la valeur de l'itérateur et passe à la cellule suivante """
        if self.iterateur is None: # on arrête l'itération à la fin
            raise StopIteration()
        else:
            valeur = self.iterateur.info # on récupère la valeur
            self.iterateur = self.iterateur.suivant # on passe au suivant
        return valeur

    def __iter__(self):
        """ Retourne l'itérateur """
        return self


class ListeIterateurInverse:
    """ Un itérateur inverse sur Liste """

    def __init__(self, commencement):
        """ Initialise l'itérateur à la cellule de commencement """
        self.iterateur = commencement

    def __next__(self):
        """ Retourne la valeur de l'itérateur et passe à la cellule précédente """
        if self.iterateur is None:
            raise StopIteration()
        else:
            valeur = self.iterateur.info # on récupère la valeur
            self.iterateur = self.iterateur.precedent # on passe au précédent
        return valeur

    def __iter__(self):
        """ Retourne l'itérateur """
        return self

# =============================    Tests   ===========================================
# Test création de liste
liste = Liste()
assert hasattr(liste, "premier")
assert hasattr(liste, "dernier")
assert hasattr(liste, "taille")
assert liste.premier is None
assert liste.dernier is None
assert len(liste) == 0

# Test empty
assert liste.empty()

# Test chainage append
apres_append = liste.append(0)
assert id(liste) == id(apres_append)

# Test 1 append
assert len(liste) == 1
assert liste.premier == liste.dernier
assert liste.premier is not None

# Test 2 append
liste.append(1)
assert len(liste) == 2
assert liste.premier.suivant == liste.dernier
assert liste.dernier.precedent == liste.premier

# Test pop
valeur = liste.pop()
assert valeur == 1
valeur2 = liste.pop()
assert valeur2 == 0
assert liste.premier is None
assert liste.dernier is None
assert len(liste) == 0

# Test iterable
assert isinstance(liste, Iterable)
iterateur = iter(liste)
assert isinstance(iterateur, Iterable)
assert isinstance(iterateur, Iterator)

# Test idempotent
assert id(iterateur) == id(iter(iterateur))

# Test iteration
liste.append(0).append(1).append(2)
tab = list(liste)
assert tab == [0, 1, 2]

# Test iteration inverse
tabInverse = list(reversed(liste))
assert tabInverse == [2, 1, 0]

# Test égalité
liste2 = Liste()
liste2.append(0).append(1).append(2)
assert id(liste2) != id(liste)
assert liste2 == liste

# Test non égal
liste3 = Liste()
liste3.append(1).append(2).append(0)
assert id(liste3) != id(liste)
assert liste3 != liste

# Test repr
assert str(liste) == "Liste((0, 1, 2))"

# Test repr eval
assert eval(str(liste)) == liste

# Test création depuis iterable
liste4 = Liste([0, 1, 2])
assert list(liste4) == [0, 1, 2]

# Test clear
liste.clear()
assert liste.empty()

# Test contains
liste.append(0).append(1).append(2)
assert 0 in liste
assert 1 in liste
assert 2 in liste
assert 42 not in liste
