Orga générale du bloc 4 du DIU : préparation visio du 29/05/2020 à 14h
======================================================================


OdJ
---

Sur la base d'une tentative de synthèse des sujets discutés sur le Mattermost

* les regroupements : dates préférées
* l'évaluation et la FOAD
* les corrections


Points de discussion et propositions
------------------------------------

### Sur les regroupements/visios/classes virtuelles/temps d'échanges synchrones

* fixer les dates : dans la semaine du 8/6, selon les créneaux plébiscités du sondage (cf. infra)
  * le calendrier avec les thèmes abordés seront fixés au plus tard début de semaine du 1/6
  * les créneaux du 29/05 et 05/06 : pour discuter organisation et besoins exprimés (d'autres contenus, des compléments, échange de références etc.)
* définir sujets abordés des visios : oui bien sûr
* pas de nouveaux contenus en visios : oui bien sûr aussi, c'est un temps de discussion et de partage des difficultés et des solutions, rien de nouveau

### Sur l'évaluation

* pour l'instant, je reste sur la formule initiale <https://forge.univ-lyon1.fr/diu-eil/bloc4#modalit%C3%A9s-de-contr%C3%B4le-des-connaissances>
* repousser à 2021 : non, ca ne fait que reculer le problème et l'établissement ne l'autorisera pas
* annuler l'évaluation : non, ce n'est ni la commande du ministère ni le contrat passé entre le rectorat et l'université, le DIU est diplomant
  * je suis favorable à trouver un moyen de dire validé/non validé au lieu des notes, mais l'établissement si oppose
* l'évaluation initialement prévue le 19/06 : accord de principe du responsable du DIU pour la repousser jusqu'au 14/7
  * je pensais à un QCM ou questions courtes en ligne, avec 5 à 10 questions sur chaque partie
    * une plateforme Lyon 1 permet à chacun de le faire quand il voudra sur une plage temporelle donnée : ça reste mon option préférée
  * alternative : un "devoir" donné à date d avec k heures pour le faire

### le FOAD

* un des deux éléments de l'évaluation
  * comme pour d'autres blocs, l'idée est de produire à plusieurs un contenu utilisable en classe en rapport avec les thèmes abordés dans le bloc 4
    * e.g. des éléments d'une séquence pédagogique pour animer une classe de Terminale
      * seul : des contenus pour 1,5h de face-à-face, binôme : 3h, trinôme : 4,5h, quadrinôme : 6h
  * l'idée est de partager ces productions (e.g., sur le gitlab) pour avoir un fond de matériel pédagogique commun aux participants
  * une feuille de calcul partagée sera mise en place avec des thèmes où vous vous inscrirez
* idem évaluation, date limite au 14/7

### Sur les corrections

* oui elle seront proposées, mais je souhaite qu'elles soient **discutées** de vive voix :
  * je ne veux pas libérer "une solution" sans explications et sans savoir où sont les difficultés
  * pour moi les regroupements de la semaine du 8 sont là pour ça, voire supra
    * ils seront enregistrés
    * les "solutions" complètes y seront livrées
    * à voir l'intérêt de vidéos complémentaires de ma part


Résultat sondage disponibilités
--------------------------------

Ici, le meilleur créneau de la semaine du 25/5 et le meilleure de celle du 1/6 et le top huit de la semaine du 8/6.

|Jour | Créneau | %|
|--- | --- | ---|
|Vendredi | 29/05/2020 14:00 à 16:00 | 47|
|Vendredi | 05/06/2020 14:00 à 16:00 | 49|
|Lundi | 08/06/2020 09:00 à 11:00 | 79|
|Lundi | 08/06/2020 14:00 à 16:00 | 77|
|Mardi | 09/06/2020 09:00 à 11:00 | 75|
|Mardi | 09/06/2020 14:00 à 16:00 | 75|
|Jeudi | 11/06/2020 09:00 à 11:00 | 79|
|Jeudi | 11/06/2020 14:00 à 16:00 | 77|
|Vendredi | 12/06/2020 09:00 à 11:00 | 79|
|Vendredi | 12/06/2020 14:00 à 16:00 | 72|


Annexes
-------

### Mail IPR du 19/05

Bonjour Mr XXX, bonjour Mr XXX

    Professeurs devant participer aux deux derniers blocs de la première vague de la formation DIU NSI,  nous nous inquiétons de n'avoir reçu, à moins de trois semaines de la date officielle de début de la formation, aucune clarification concernant notre situation vis à vis de nos établissements scolaires respectifs pendant la période de formation.

    Initialement nous étions convoqués sur dix journées en présentiel et à côté de cela étions dispensés des tâches liées au passage du baccalauréat, ce que justifiaient parfaitement le niveau et l'intensité de la formation. Ce DIU étant sanctionné par un examen, il nécessite des journées pour réviser et construire des séquences sur lesquelles nous seront aussi évalués.

    Aujourd'hui, les formations concernant les enseignants ayant été déclarées suspendues et une reprise des cours en présentiel en lycée paraissant de plus en plus probable, nous nous trouvons face à la perspective d'avoir à assurer physiquement et à distance notre service d'enseignement jusqu'au 4 Juillet, tout en devant suivre la formation à distance, sans avoir aucune indication sur ce que seront exactement nos obligations de service pendant cette période.

    Or quels que soient les aménagement de service qui nous seront accordés pour que nous puissions effectivement bénéficier de la formation, ils vont demander, de notre part et de celle de nos établissements, une organisation devant être envisagée avec un peu d'avance.

    Il nous paraît donc indispensable que l'information à ce sujet soit communiquée rapidement et sans ambiguïté aussi bien à nos établissements qu'à nous-mêmes. Nous demandons donc à recevoir rapidement un nouvel ordre de mission précisant clairement les périodes sur lesquelles nous serons dégagés de nos services d'enseignement.


    Nous sommes bien conscients que le pouvoir de décision en la matière ne vous appartient pas, mais vous êtes les seuls interlocuteurs à qui nous pouvons communiquer notre préoccupation grandissante, avec l'espoir que vous pourrez la relayer à qui de droit.

    Bien cordialement

### Réponse IPR du 21/05


    Mesdames et messieurs les chefs d'établissement

    Au moins un professeur de votre établissement est inscrit au DIU qui permet d'obtenir une autorisation d'enseigner la spécialité NSI. Cette formation aura bien lieu en juin, uniquement en distanciel.

    Les professeurs de la vague 1 ont dix jours de formation, la semaine du 8 juin et la semaine du 22 juin, ceux de la vague 2 ont cinq jours de formation la semaine du 15 juin, ce qu'a validé la DFIE, les OM sont en cours d'envoi.

    L'obtention de ce DIU revêt une importance capitale pour ces professeurs qui se destinent à enseigner NSI en première ou terminale l'an prochain.

    Par conséquent, si une reprise des cours en lycée a lieu en juin, il sera important de tenir compte de cette contrainte supplémentaire.

    Comptant sur votre collaboration dans cette période difficile, nous vous prions de recevoir l'expression de nos plus sincères salutations 


###  Communication du 27/05

    Bonsoir,
    je me permets de relancer ici, avec vous, un sujet qui a été abordé rapidement sur notre mailing list. Désolée d'avance de la longueur de mon message, peu adaptée au format forum, mais je souhaitais que tout le monde puisse avoir accès à cet échange.
    Tout d'abord, je voudrais vous remercier pour tous les efforts pour rendre cette formation en distanciel la plus accessible possible pour nous.

    Concernant les séances en visio, vous avez proposé un Doodle à remplir avant ce soir, avec des créneaux à partir de ce jeudi 28 mai, jusqu'au 12 juin.
    Tout d'abord, comme il a été évoqué par d'autres, à quelques jours près, ça tombe assez mal : nous attendons tous les déclarations d'Edouard Philippe, et les décisions administratives qui s'en suivront, pour savoir comment vont s'organiser pour nous les semaines à venir.

    Par ailleurs, pour envisager des visios un peu efficaces, on va définir des parties du programme à aborder à chaque séance. Participer aux visios exigera d'avoir au moins lu les cours et les sujets de TD/TP (voire essayé de les faire) pour avoir une chance d'être utile. Or, nous sommes nombreux à être encore pleinement pris par le travail d'établissement actuellement : les cours à distance ou en présentiel (ceux qui ont des 6e-5e ont déjà repris), les devoirs à corriger, les bulletins à remplir, les conseils de classe en visio ou en présentiel, les éventuelles réunions diverses pour élaborer des plans de reprise, et j'en oublie forcément. Cela est censé durer jusqu'au 3 juillet, mais cela va quand même se tasser d'ici 15 jours, alors que pendant encore un moment, nous sommes clairement à 100% (je n'ose pas dire à 200%, mais il est probable qu'un certain nombre de collègues n'aient pas leur compte de sommeil actuellement).
    A partir du 8 juin : nous sommes censés être déchargés de cours, et même s'il nous restera des tâches à faire, le rythme sera ralenti, et nous avons une meilleure chance de pouvoir nous concentrer sur les cours.
    Certains (dont je fais partie, mais je ne suis pas la seule), ne pourront pas regarder les cours avant.. disons... le samedi 6 juin. Mais si les visios ont commencé avant, on risque de se retrouver en décalage.
    Il paraitrait plus cohérents de commencer les visios le 8 juin. Ceux qui peuvent s'avancer le font, et nous vous remercions infiniment d'avoir commencé à publier les cours, et ils doivent pouvoir poser des questions sur le forum... Mais fixer des visios (et donc des échéances communes d'avancée du travail) avant le 8 juin risque de fortement augmenter la pression que nous subissons déjà (ou nous mettons nous-mêmes). Ma proposition est donc de garder pour l'instant les forums, et de ne commencer les visios qu'à partir du 8 juin. Tant qu'à étaler, proposer encore des visios après le 12 juin me paraîtrait en revanche une piste intéressante...
    J'ai évoqué hier soir sur la mailing list le fait de vous faire cette demande, et j'ai à l'heure actuelle une douzaine de réponses, toutes positives. Je laisse à ceux qui souhaiteraient faire d'autres propositions le soin de répondre ici...

    Même à partir du 8 juin, il est certain que nous ne pourrons pas toujours être tous présents, mais en ayant défini un planning avec le programme de chaque séance, cela nous permettra d'anticiper : de savoir qu'il faut poser nos questions sur le forum si on sait qu'on ne peut pas être présent à la visio. ; de définir aussi nos priorités en terme de besoins (pour beaucoup, il va s'agir aussi d'équilibrer entre cours du DIU et garde d'enfants, je sais que je me permettrai de zapper les visios sur les cours que je comprendrai bien pour pouvoir ensuite laisser mon compagnon gérer quand j'ai une visio à ne pas manquer...).

    J'ai donc une autre demande : pourriez-vous vous assurer que les visios sont bien "en plus", pour répondre aux questions, et qu'aucun contenu inédit n'y soit évoqué ? (L'idée n'est pas de se censurer, mais si un contenu inédit est évoqué, s'assurer d'ajouter un note pour l'ajouter dans les cours publiés). En effet, il faut s'assurer que tout collègue qui ne pourrait pas être présent à la visio, ait néanmoins tous les contenus dans les cours publiés, s'il les suit de manière autonome...
    Merci d'avance d'être vigilent sur ce point.

    Pour terminer, pourriez-vous nous préciser les modalités, et surtout les délais d'évaluation ?
    Sur ce point, les avis divergent : certains d'entre nous auraient souhaité que les évaluations soient tout bonnement abandonnées (nous ne sommes pas des étudiants de 19 ans, nous suivons cette formation car nous sommes volontaires, il est inutile de jouer de la carotte et du bâton pour nous forcer à travailler). Considérant que les examens sont obligatoires pour définir un DIU, certains auraient souhaité que les examens soient reportés à juin 2021, afin de pouvoir étaler dans le temps notre formation en la terminant de manière autonome, et même de progresser en enseignant ces notions en Terminale l'an prochain. D'autres collègues reconnaissent qu'ils souhaitent en finir avant l'été, tout en espérant qu'il sera tenu compte des circonstances exceptionnelles pour alléger le mode d'évaluation à ce qui sera considéré comme le strict minimum administratif...
    Ma recherche de compromis me ferait demander s'il est possible de proposer, au choix, de passer les examens en juin 2020, ou en juin 2021 avec la 2e vague de collègues qui suivent la formation, même si nous suivons la formation cette année, mais je ne sais pas si c'est possible et/ou une idée qui intéresserait les collègues.

    Je me permets néanmoins de repréciser que, même si les cours sont déjà publiés, il sera difficile pour beaucoup d'entre nous de nous y consacrer avant le 8 juin, et que dans les circonstances exceptionnelles dans lesquelles nous sommes, même officiellement déchargés, il sera difficile de nous y consacrer autant que nous l'aurions normalement fait (enfants qui ne sont pas à l'école, élèves que l'on n'aura pas eu le cœur de réellement abandonner et qu'on suivra encore malgré la décharge...), donc nous apprécierions que vous nous laissiez le maximum de temps que votre organisation pourra permettre.

    Je vous remercie de m'avoir lue jusqu'au bout.
    Si les collègues souhaitent ajouter des choses, je les y invite.
    Bien cordialement,

    PS : je vais remplir le Doodle en cohérence : rien avant le 8 juin, tout disponible à partir du 8 juin, même je n'ai en réalité aucune visibilité sur mes dispos la semaine du 8...

### Communication du 28/05, complément à la précédente


    Bonjour, comme l'a écrit XXX et cela a été dit par la majorité des collègues, j'apprécie tous les efforts d'adaptation dans l'organisation de ce DIU. Il nous manque cet ordre de mission qui nous dirait : le mois de juin est consacré exclusivement à votre formation DIU, Elle stipulerait : on vous garde vos enfants et vos élèves sont accueillis par des professeurs qui ont du temps. Comme tout ceci n'est qu'un rêve, il faut en effet que chacun puisse avancer à son rythme. Je comprends les collègues qui lisent les posts de cette plateforme et qui se disent "j'ai loupé tout ça ! ". Alors on n'a pas tous le même niveau, mais l'idée d'une correction basiques des exos sous forme de vidéo me semble bien adaptée pour que chacun puisse avancer en fonction du temps dont il dispose. Désolé pour les formateurs, je sais que cela demande un travail supplémentaire. Concernant l'examen, je suis partisan d'un examen simple en juin dans la plus grande bienveillance. Je n'imagine pas revenir la-dessus l'an prochain après une année à faire les nouveaux programmes de terminale. Dernière remarque : je comprends les collègues épuisés par cette année et pense qu'une adaptation est nécessaire. Comme me l'a si joliment dit mon inspectrice en février : "Nous pensions que les burn-out n'arriveraient qu'à partir de janvier 2020. Nous sommes étonnés que l'hécatombe ait débuté dès octobre". Prenons-soin de nous. 
