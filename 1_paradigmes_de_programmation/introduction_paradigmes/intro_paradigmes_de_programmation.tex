\documentclass[table]{beamer}
\usepackage[french]{babel}
\usepackage{tikz}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{beamerthemesplit}
\usetheme{AnnArbor}
\usecolortheme{beaver}
\usefonttheme{professionalfonts}


\usepackage{wasysym}
\usepackage{times}
\usepackage{subfigure}
\usepackage{fancyvrb}
\usepackage{booktabs,multirow,rotating, tabularx}
\usepackage{graphicx}
\usepackage{listings}

\lstset{ 
  captionpos=b,                    % sets the caption-position to bottom
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breaklines=true,                 % sets automatic line breaking
%  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
%  numbersep=5pt,                   % how far the line-numbers are from the code
%  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
%  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  tabsize=2,	                   % sets default tabsize to 2 spaces
  title=\lstname,                   % show the filename of files included with \lstinputlisting; also try caption instead of title
  inputencoding=utf8,
  literate={é}{{\'e}}1 {ê}{{\"e}}1 {è}{{\`e}}1 {î}{{\^i}}1 {à}{{\`a}}1 {ù}{{\`u}}1 {ç}{{\c c}}1 
}

%------------------------------------------------------------------------------------

\hypersetup{
    pdfauthor   = {Romuald THION},%
    pdftitle    = {DIU-EIL Bloc 4},%
    pdfsubject  = {Le modèle relationnel},%
    pdfkeywords = {DIU, bases de données, algèbre relationnelle},%
    colorlinks  = false,
    linkbordercolor = blue, % hyperlink borders will be red
    pdfborderstyle = {/S/U/W 1}% border style will be underline of width 1pt
}	

\title[DIU-EIL Bloc 4 -- Paradigmes]{
    \textsc{DIU-EIL Bloc 4\\ Langages de programmation: un panorama}
}

\author[R. THION, E. COQUERY]{
    Romuald THION, Emmanuel COQUERY
}
\institute{ 
{  
    \url{https://forge.univ-lyon1.fr/diu-eil/bloc4}}
}

%\date{Mardi 21 juin 2011 (matin)}

\newcommand{\bleu}[1]{{\color{cyan} #1}}
%-------------------------------------------------------------------------------

\usefoottemplate{%
  \vbox{% 
    \tinycolouredline{black}%
      {\color{white}\insertshortauthor\hfill\insertshorttitle\hfill\insertframenumber}%
}}

%\usefoottemplate{}

\setbeamertemplate{navigation symbols}{}
\beamertemplatetransparentcovereddynamicmedium 

\AtBeginSection[]{
  \begin{frame}%{Sommaire}
  \tableofcontents[currentsection, hideothersubsections]
  \end{frame} 
}




\begin{document}
	\begin{frame}
		\titlepage
	\end{frame}

    \begin{frame}{Plan}
        \tableofcontents[hideallsubsections]
    \end{frame} 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
  \frametitle{Généralités sur les langages de programmation}

  Un langage de programmation est défini par
  \begin{itemize}
    \item une \alert{syntaxe} 
    \begin{itemize}
      \item en générale textuelle, mais il en existe des graphiques et des formelles
    \end{itemize}
    \item une \alert{sémantique} expliquant l'exécution d'un programme;
  \end{itemize}
  \pause

  Pour son évaluation il peut être
  \begin{itemize}
    \item \alert{interprété}:
      \begin{itemize}
        \item un interpréteur va se charger de son exécution
      \end{itemize}
    \item \alert{compilé}:
      \begin{itemize}
      \item un compilateur va se charger de sa
        traduction, soit en langage machine, soit dans un autre langage
        interprété (ex: dans le cas des machines virtuelles)
      \end{itemize}
  \end{itemize}
  Dans tous les cas, des vérifications vont être effectuées pour minimiser le nombre d'erreurs à l'exécution  (syntaxe, définitions, typage, etc.)
\end{frame}


\begin{frame}
  \frametitle{Paradigmes de programmation}
  \begin{center}
  \itshape
      \href{https://fr.wiktionary.org/wiki/paradigme}{Paradigme} : représentation du monde ; manière de voir les choses ; modèle cohérent de pensée, de vision du monde qui repose sur une base définie, sur un système de valeurs. 
  \end{center}
  
  \begin{itemize}
  \item 
    Façon d'exprimer le résultat / l'effet désiré pour un programme:
    \begin{itemize}
    \item par une liste d'instructions
    \item par des expressions à évaluer
    \item comme devant respecter une certaine spécification
    \item par transformation successives
    \item \dots
    \end{itemize}
  \item Le paradigme a une influence forte sur la manière de concevoir un
    programme
    \begin{itemize}
    \item il constitue un \alert{des critères principaux} pour le choix d'un langage
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{Quels paradigmes de programmation ?}
\begin{center}
    \begin{figure}
      \centering
      \includegraphics[width=0.9\linewidth]{./img/paradigmes_vanroy.png}%
    \end{figure}
    Peter Van Roy
\end{center}
\end{frame}


\begin{frame}{\insertsection}
  
  \begin{alertblock}{Quelques remarques générales}
    \begin{itemize}
    
      \item Les langages sont \alert{très rarement} \og{}\emph{purs}\fg{}, ils intègrent plusieurs paradigmes.
      \item La taxonomie n'est pas consensuelle, voir par exemple \href{https://en.wikipedia.org/wiki/Programming_paradigm}{Wikipedia}
    \end{itemize}
    
    
  \end{alertblock}

  
  \begin{exampleblock}{Le cas de Python \url{https://docs.python.org/3/}}
  \begin{itemize}
    \item Impératif : \href{https://docs.python.org/3/reference/simple_stmts.html}{The Python Language Reference : 7. Simple statements} et \href{https://docs.python.org/3/reference/compound_stmts.html}{8. Compound statements}

    \item Fonctionnel : \href{https://docs.python.org/3/howto/functional.html}{Functional Programming HOWTO}
    
    \item Objet : \href{https://docs.python.org/3/tutorial/classes.html}{The Python Tutorial : 9. Classes}
  \end{itemize}
     

  \end{exampleblock}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}
%  \frametitle{Plan}

%  \tableofcontents
%\end{frame}

%\AtBeginSection[]
%{
%  \begin{frame}
%    \frametitle{Plan}
%    \tableofcontents[currentsection]
%  \end{frame}
%}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Langages impératifs}
\begin{frame}
  \frametitle{Généralités}
  \begin{itemize}
  \item Principe:
    \begin{itemize}
    \item Un programme est un \alert{ensemble d'instructions} (actions à réaliser) et de déclarations (fonctions, procédures, etc).
    \item L'exécution du programme correspond à celle des instructions.
    \end{itemize}
  \item Une variable correspond à un \alert{emplacement mémoire} dans lequel on range / on lit  une valeur.
  \item Les calculs sont réalisés à travers ces instructions
    \begin{itemize}
    \item même s'il existe des notions d'expression et de fonctions
    \end{itemize}
  \end{itemize}
  
  \begin{center}
    \alert{Mots-clefs} : affectation, mémoire, instruction (\emph{statement}), boucle, mutabilité, effets de bord (\emph{side effects})
    
    \alert{Exemples} : C (paradigmatique), bash, Pascal, Python, Java, ASM, \ldots
  \end{center}
  % on dit aussi (procéduraux)
\end{frame}

\begin{frame}
  \frametitle{Usages typiques}
  \begin{itemize}
  \item Lorsque le programme à réaliser s'exprime sous forme \alert{d'actions à réaliser}
    \begin{itemize}
    \item Les actions reflètent les changements dans l'environnement (mémoire) du programme
%    \item Des calculs peuvent être effectués à travers un ensemble d'instructions reflétant la méthode de calcul
    \end{itemize}
  \item Couramment utilisé dans le cadre de la \emph{programmation système}
    \begin{itemize}
      \item Pour une gestion manuelle, potentiellement fine, de l'allocation mémoire
%      \item Le langage machine et l'assembleur sont des langages
        impératifs
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Un exemple filé : le PGCD}

\begin{center}
  L'algorithme d'Euclide pour le calcul du Plus Grand Commun Diviseur (PGCD, \emph{gcd} en anglais), voir \href{https://en.wikipedia.org/wiki/Euclidean_algorithm#Implementations}{Wikipedia} et \href{http://rosettacode.org/wiki/Greatest_common_divisor}{rosettacode.org}
\end{center}

{\scriptsize
\begin{verbatim}
function gcd(a, b)
    while b != 0
        t := b
        b := a mod b
        a := t
    return a

function gcd(a, b)
    gcd(a, 0) = a
    gcd(a, b) = gcd(b , a mod b)

function gcd(a, b)
    if b = 0
        return a
    else
        return gcd(b, a mod b)
\end{verbatim}
}


\end{frame}


\begin{frame}
  \frametitle{PGCD en Python}
  \lstinputlisting[language=Python]{./code/pgcd.py}
\end{frame}


\begin{frame}
  \frametitle{PGCD en C}
  \lstinputlisting[language=C]{./code/pgcd.c}
\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Langages fonctionnels}
\begin{frame}
  \frametitle{Généralités}
  \begin{itemize}
  \item Principe:
    \begin{itemize}
      \item Un programme fonctionnel est un \alert{ensemble de déclarations}
      (fonctions, etc) suivi d'une expression;
      \item l'exécution du programme consiste à \alert{évaluer l'expression}.
      \item Pas d'affectation\footnote{$v = expr$ \alert{définit} la variable $v$, \emph{une et une seule fois}} : les variables sont des \alert{résultats intermédiaires} dans le calcul.
      \item Gestion de la mémoire automatisée de l'allocation mémoire
    \end{itemize}

  \item Ordre supérieur: 
    \begin{itemize}
    \item les fonctions sont des \og{}\alert{citoyennes de première classe}\fg{}, manipulables au même titre que les autres valeurs :
    \begin{itemize}
      \item une variable peut prendre une fonction pour valeur,
      \item une fonction peut prendre en argument une autre fonction,
      \item une fonction peut renvoyer une fonction comme résultat\footnote{quitte à la \og{}fabriquer\fg{} au passage avec une $\lambda$ expression par exemple}
    \end{itemize}
      
    \end{itemize}
%    
%      \begin{itemize}
%      \item En particulier liée à la notion de fermeture (\emph{closure})
%      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Usages typiques}
%  \frametitle
  
  \begin{center}
   \alert{Mots-clefs} : déclaratif, expressions, immutabilité, ordre supérieur, $\lambda$-calcul
    
    \alert{Exemples} : Haskell (paradigmatique), Lisp, Scheme, OCaml, XQuery\ldots
  \end{center}


  \begin{itemize}
    \item Manipulations de haut niveau
    \item Calculs algébriques, abstraits
    \item Analyse de structures
    \item Transformations complexes
    \item Écriture de compilateurs
  \end{itemize}

\end{frame}


\begin{frame}
  \frametitle{PGCD récursif en Python}
  \lstinputlisting[language=Python]{./code/pgcd_rec.py}
\end{frame}

\begin{frame}
  \frametitle{PGCD récursif en C}
  \lstinputlisting[language=C]{./code/pgcd_rec.c}
\end{frame}

%\begin{frame}
%  \frametitle{PGCD en OCAML}
%  \lstinputlisting[language=Caml]{./code/pgcd.ml}
%\end{frame}

\begin{frame}
  \frametitle{PGCD en Haskell}
  \lstinputlisting[language=Haskell]{./code/pgcd.hs}
\end{frame}


\begin{frame}
  \frametitle{Ordre supérieur en Python -- 1}
  \lstinputlisting[language=Python]{./code/fun_prog.py}
\end{frame}

\begin{frame}
  \frametitle{Ordre supérieur en Python -- 2}
  \lstinputlisting[language=Python]{./code/fun_prog2.py}
\end{frame}


\begin{frame}
  \frametitle{Ordre supérieur en Python -- 3}
  \lstinputlisting[language=Python]{./code/fun_prog3.py}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Langages orientés objet}
\begin{frame}
  \frametitle{Généralités}
  \begin{itemize}
  \item Objet:
    \begin{itemize}
    \item \alert{ensemble de champs} (ou de variables, ou de valeurs, etc)
      auquel on associe des \alert{méthodes};
    \item une méthode est un comportement associé à l'objet, typiquement une fonction ou une procédure;
    \item une méthode peut accéder aux champs de l'objet auquel elle est rattachée : c'est son \alert{contexte d'exécution}
    \end{itemize}
  \item Un langage est dit Orienté Objet (OO) lorsqu'il intègre une notion d'objet
    \begin{itemize}
    \item ce langage peut par ailleurs être impératif ou fonctionnel
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Classes et héritage}
  \begin{itemize}
  \item Classe:
    \begin{itemize}
    \item \alert{Patron} permettant de fabriquer des objets
      \begin{itemize}
      \item notion présente dans de nombreux langages OO
      \end{itemize}
    \end{itemize}
  \item Héritage: 
    \begin{itemize}
    \item une classe \emph{fille} \alert{hérite} d'une classe \emph{mère}
    \item les méthodes et les champs de classe mère sont
      \alert{automatiquement définis dans la classe fille}
    \item la classe fille \emph{peut} avoir des champs et des méthodes supplémentaires
    \item la classe fille \emph{peut} changer la définition de certaines
      méthodes provenant de la classe mère
    \item toute instance de la classe fille peut être utilisée là où une instance de la classe mère est attendue (sous-typage)
    \end{itemize}
%  \item Sous-typage:
%    \begin{itemize}
%    \item si A est un sous-type de B, toute valeur de type A peut être
%      utilisée là où une valeur de type B est attendue
%    \item les objets définis à partir d'une classe fille ont un type
%      qui est un sous-type du type des objets fabriqués à partir de la
%      classe mère correspondante
%    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  Usages:
  \begin{itemize}
  \item Avantageux dans le cadre du développement de \alert{gros logiciels}:
    \begin{itemize}
    \item réutilisation du code
    \item découpage et isolation du code (encapsulation)
    \item donne lieu à des méthodes de conception dédiées (UML), en particulier dans le développement des systèmes d'information
    \end{itemize}
  \end{itemize}
  
  
  \begin{center}
    \alert{Mots-clefs} : classes ou prototype, encapsulation, héritage
    
    \alert{Exemples} : C++ et Java (paradigmatiques), Python, OCaml, \ldots
  \end{center}

  
%  Quelques langages orientés objet:
%  \begin{itemize}
%  \item C++, Java, C\texttt{\#}
%  \item OCAML, Scala
%  \item Javascript, Python , Ruby
%  \item etc
%  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Exemples de classes en Python}
  \lstinputlisting[language=Python, firstline=1, lastline=13]{./code/classes_personnes.py}
  % personne, professeur (matière), élève(classe)
\end{frame}
\begin{frame}
  \frametitle{Exemples de classes en Python - 2}
  \lstinputlisting[language=Python,  firstline=15, lastline=27]{./code/classes_personnes.py}
  % personne, professeur (matière), élève(classe)
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Autres langages}

%\subsection{\TeX et \LaTeX}
%\begin{frame}
%  \frametitle{\TeX et \LaTeX}
%  \begin{itemize}
%  \item Langage de rédaction de documents;
%  \item intègre des notions de macros ($\sim$ fonctions) et de variables;
%  \item mélange le texte à produire et l'utilisation des macros pour
%    la structuration et la mise ne forme.
%  \end{itemize}
%%  \lstinputlisting[language=TeX]{./code/tex-example}
%\end{frame}

%\subsection{XML}
%\begin{frame}
%  \frametitle{XML (eXtensible Markup Language)}
%  \begin{itemize}
%  \item Format de documents permettant de représenter des arbres sous
%    forme textuelle;
%  \item syntaxe à balise:
%    \begin{itemize}\item 
%      XHTML est la version XML de HTML;
%    \end{itemize}
%  \item standard adopté par tous les grands acteurs de l'informatique
%  \item \emph{ne dit pas comment interpréter les arbres représentés}
%  \end{itemize}
%  \texttt{
%  \begin{tabbing}
%    <el\=eve\=>\\
%    \><nom>Toto</nom>\\
%    \><prenom>Dupond</prenom>\\
%    \><note valeur="10">\\
%    \>\><matiere>mathematiques</matiere>\\
%    \></note>\\
%    </eleve>
%  \end{tabbing}}
%\end{frame}

%\subsection{Expressions régulières}
%\begin{frame}
%  \frametitle{Expressions régulières}
%  \begin{itemize}
%  \item Permet de décrire des ensembles de chaînes de caractères
%    \begin{itemize}
%    \item avec des algorithmes pour reconnaître les chaînes leur appartenant
%    \end{itemize}
%  \end{itemize}
%  \begin{tabular}{cl|cl}
%    \texttt{a} & un \texttt{a}& a+ & au moins un \texttt{a}\\
%    a?& au plus un \texttt{a}&a*& n'importe quel nombre de \texttt{a}
%    \\
%    \texttt{a $\mid$ b}& un \texttt{a} ou un \texttt{b} &
%    \texttt{[adf]}& un \texttt{a} ou un \texttt{d} ou un \texttt{f} \\
%    \texttt{[a-t]}&une lettre entre \texttt{a} et \texttt{t} &
%    \texttt{[\^{}a]}&toute lettre sauf \texttt{a}\\
%    $e_1e_2$& \multicolumn {3}{l}{mot reconnu par $e_1$, suivi d'un mot reconnu par
%    $e_2$}\\$(e)$&\multicolumn {3}{l}{mot reconnu par $e$}
%  \end{tabular}\\[2ex]
%  \texttt{(([1-9][0-9]*)|0)(.[0-9]*[1-9])?}\\~\hfill nombres décimaux sans chiffre non significatif
%\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Langages logiques}
%\begin{frame}
%  \frametitle{Généralités}
%  \begin{itemize}
%  \item Principe:
%    \begin{itemize}
%    \item Un programme est un \alert{ensemble de formules logiques};
%    \item l'exécution du programme consiste à \alert{trouver les valeurs} pour les variables rendant la formule \alert{vraie}.
%    \end{itemize}
%  \item Une variable correspond à une inconnue mathématique.
%  \item Mécanisme d'\alert{unification} différent d'une affectation
%  \item Exploration des différentes possibilités:
%    \begin{itemize}
%    \item  implicite;
%    \item ou bien facilitée par des mécanismes tels que le retour
%      arrière (backtrack) intégrés au langage.
%    \end{itemize}
%  \end{itemize}
%\end{frame}
%\begin{frame}
%  %\frametitle{Usages}
%  Usages:
%  \begin{itemize}
%  \item Intelligence artificielle
%  \item Optimisation
%  \item Requêtes
%  \end{itemize}
%  \vfill
%  Quelques langages logiques:
%  \begin{itemize}
%  \item Prolog, avec contraintes (IA)
%  \item SQL (Requêtes)
%  \end{itemize}
%\end{frame}
%\begin{frame}
%  \frametitle{PGCD en Prolog}
%  %\verbatiminput{./code/pgcd.pl}
%  \lstinputlisting[language=Prolog]{./code/pgcd.pl}  
%  
%  \texttt{?- pgcd(132,105,X).}
%  \begin{flushright}
%%    \texttt{:-} correspond à $\Leftarrow$\\
%%    \texttt{,} correspond à $\wedge$\\
%%    \texttt{!} est une notation extra-logique (pas d'autre possibilité)
%  \end{flushright}
%\end{frame}
%\begin{frame}
%  \frametitle{Requête SQL}
%  Dans une base de données relationnelle:
%  \lstinputlisting[language=Prolog]{./code/query.sql} 
%%  \texttt{
%%  \begin{tabbing}
%%    SELECT eleve.nom, eleve.prenom\\
%%    FROM inscrit, eleve\\
%%    WH\=ERE inscrit.num = eleve.num\\
%%       \>AND inscrit.sport = 'Volley'\\
%%       \>AND eleve.niveau = '3eme'
%%  \end{tabbing}}

%En Tuple/Domain Relational Calculus :

%$\{e.nom,e.prenom\mid \exists s ~ eleve(e)\wedge inscrit(i)\wedge$\\$\qquad
%e.num = i.num\wedge i.sport=\texttt{'Volley'}\wedge e.niveau=\texttt{'3eme'}\}$
%$\{x,y\mid \exists n\exists z \exists s ~ eleve(z,x,y,n) \wedge
%inscrit(z,s)\wedge$\\$\qquad  s=\texttt{'Volley'}\wedge n=\texttt{'3eme'}\}$

%% Relations:
%% \begin{itemize}
%% \item eleve(num,nom,prenom,niveau)
%% \item inscrit(num,sport)
%% \end{itemize}
%% Eleves de 3eme inscrits au volley-ball
%\end{frame}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Langages à base de règles}
%\begin{frame}
%  \frametitle{Généralités}
%  \begin{itemize}
%  \item Souvent intégrés avec un autre langage
%  \item Principe:
%    \begin{itemize}
%    \item un programme est un \alert{ensemble de règles de transformation};
%    \item son exécution est l'application de ces règles sur une
%      entrée.
%    \end{itemize}
%  \item Comme pour les langages OO, s'appuie sur un autre paradigme
%    (impératif, fonctionnel, etc)
%  \item les objets/valeurs/structures transformés par les règles
%    dépendent du langage
%    \begin{itemize}
%    \item des ensembles de faits, des arbres de données, des objets
%      (au sens programmation OO), etc
%    \end{itemize}
%  \end{itemize}
%\end{frame}
%\begin{frame}
%  \frametitle{Quelques langages à base de règles}
%  \begin{itemize}
%  \item XSLT: XML Stylesheet Language Transformations (fonctionnel
%    sans ordre supérieur)
%    \begin{itemize}
%    \item Transformation de document, en général à des fin de
%      présentation et/ou d'interopérabilité
%    \end{itemize}
%  \item CHR: Constraint Handling Rules (logique)
%    \begin{itemize}
%    \item Calculs par réécriture de faits (contraintes)
%    \end{itemize}
%  \item Drools (JBoss Rules) (impératif, objet)
%    \begin{itemize}
%    \item Écriture de règles métier, de politique de contrôle d'accès
%      à des données
%    \end{itemize}
%  \end{itemize}
%\end{frame}
%\begin{frame}
%  \frametitle{PGCD en CHR}
%  \verbatiminput{./code/pgcd.chr}
%\end{frame}

\end{document}
