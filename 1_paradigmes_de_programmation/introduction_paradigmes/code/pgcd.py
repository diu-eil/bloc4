# traduction directe du pseudo-code
def pgcd_imperatif(a, b):
  while (b != 0):
    t = b
    b = a%b
    a = t
  return a

# version "Pythoniste", avec l'idiome du swap
def pgcd_pythoniste(a, b):
  while b:
    a, b = b, a%b
  return a
  
# version "des développeurs"
import gcd from math
