ma_liste = [1, 2, 0, 4, 3]

# on peut affecter une fonction a une variable
def carre(x):
  return x*x
g = carre

# la fct standard map prend une fct en parametre et une liste
# elle applique cette fonction à tous les éléments
# map(f, [a, b, c, ...]) = [f(a), f(b), f(c), ...]
# https://docs.python.org/3/library/functions.html#map
mes_carres =  map(g, ma_liste)
print(list(mes_carres))

# on peut passer une lambda expression
mes_carres_lambda =  map(lambda x: x*x, ma_liste)
print(list(mes_carres_lambda))
