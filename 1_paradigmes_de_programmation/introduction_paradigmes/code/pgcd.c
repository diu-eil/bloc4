/* gcc -Wall pgcd.c && ./a.out */
#include <stdio.h>

int pgcd(int a, int b) {
  int t;
  while (b != 0){
    t = b;
    b = a % b;
    a = t;  
  }
  return a;
}

int main() {
  printf("pgcd de %d et %d: %d\n", 84, 18, pgcd(84,18));
}
