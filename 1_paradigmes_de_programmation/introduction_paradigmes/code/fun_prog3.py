# une variante où on a "curryfiée" la composition :
#   * comp n'attend pas 2 paramètres mais un seul (f)
#   * comp(f) retourne une fonction qui attend un paramètre (g)
#   * comp(f)(g) retourne enfin une fonction qui attend un paramètre (x)
comp = lambda f : lambda g : lambda x : f(g(x))

# donc on écrit comp(f)(g), noter les parenthèses supplémentaires
g = comp(lambda x: x + 1)(lambda x: x * x)
print(g(3))
