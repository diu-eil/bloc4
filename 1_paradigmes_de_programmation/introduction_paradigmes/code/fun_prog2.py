# on peut créer des fonctions et les retourner
def compose(f, g):
  return (lambda x: f(g(x)))
  
f = compose(lambda x: x + 1, lambda x: x * x)
print(f(3))

# on peut aussi définir la composition uniquement
# avec des expressions fonctionnelles (lambda)
comp = lambda f, g : lambda x : f(g(x))

g = comp(lambda x: x + 1, lambda x: x * x)
print(g(3))
