class Personne:
    def __init__(self, un_nom, un_prenom):
        self.nom = un_nom
        self.prenom = un_prenom
    def affiche(self):
        print(self.prenom + " " + self.nom)

class Eleve(Personne):
    def __init__(self, un_nom, un_prenom, un_groupe):
        Personne.__init__(self, un_nom, un_prenom)
        self.groupe = un_groupe
    def affiche(self):
        print (self.prenom + " " + self.nom + ": " + self.groupe)

class Prof(Personne):
    def __init__(self, un_nom, un_prenom, une_matiere):
        Personne.__init__(self,un_nom,un_prenom)
        self.matiere = une_matiere
    def affiche(self):
        print (self.prenom + " " + self.nom + " (" + self.matiere +")")

un_gars  = Personne('Thion', 'Romuald')
un_eleve = Eleve('Hunting', 'Will', '1ereA')
un_prof  = Prof('Maguire','Sean.','Mathematiques')

for p in [un_gars, un_eleve, un_prof]:
    p.affiche()
