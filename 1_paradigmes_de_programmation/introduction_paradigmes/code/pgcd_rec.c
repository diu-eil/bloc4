#include <stdio.h> 
  
int pgcd(int a, int b) 
{ 
    if (b == 0) 
        return a; 
    return pgcd(b, a%b); 
} 

int main() {
  printf("pgcd de %d et %d: %d\n", 84, 18, pgcd(84,18));
}
