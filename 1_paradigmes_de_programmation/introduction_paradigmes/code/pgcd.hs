-- avec filtrage de motif : la définition au 
-- plus proche de celle mathématique
pgcd :: Integer -> Integer -> Integer
pgcd a 0 = a
pgcd a b = pgcd b (a `mod` b)

a, b :: Integer
a = 132
b = 105

main :: IO ()
main = putStrLn $ "le pgcd de " ++ show a ++ " et " ++ show  b ++ " est "  ++ show  (gcd a b) ++ "\n"
