# DIU bloc 4 : "Paradigmes de programmation"

## Objectifs pédagogiques

Extrait du programme du DIU :

> Paradigmes de programmation
>
> - Programmation impérative
> - Programmation fonctionnelle
> - Programmation objet
> - Introduction à la programmation événementielle et à la programmation parallèle
> - ~~Programmation logique~~ (retiré du programme DIU V2)

## Contenus/programmes

### Le cours

Les paradigmes impératifs, fonctionnels et objets

- [planches d'introduction](intro_paradigmes_de_programmation.pdf)
- La [vidéo YT de la partie 1](https://youtu.be/Oz6eIJU4m2c) (introduction et impératif)
- La [vidéo YT de la partie 2](https://youtu.be/z0UbdRVTql4) (fonctionnel et objet)
- [planches POO en Python](04-Programmation_orientée_objet.pdf)

Retrouvez tous les extraits de code du support de cours dans [dans le dossier](introduction_paradigmes/code/).

### Le TP

- un premier TP centré sur la [programmation fonctionnelle en Python](TP/TP_funct.py) est en ligne

### Correction

- Voir [dans la branche correction du Gitlab](https://forge.univ-lyon1.fr/diu-eil/bloc4/-/tree/correction/1_paradigmes_de_programmation/TP/correction)

## Références

- <http://rosettacode.org> **très utile**
- <https://insights.stackoverflow.com/survey/2019#technology>
- <https://www.tiobe.com/tiobe-index/>
- Pour les décorateurs <https://wiki.python.org/moin/PythonDecoratorLibrary>
