Notes sur les travaux pratiques
===============================


Vidéo de prise en main de SQLite <https://youtu.be/MxyE7rnta5w>

Commandes utiles (non SQL)  de sqlite
--------------------------------------

* `.help` pour la liste des commandes
* `.exit` pour quitter
* `.mode column` pour changer le mode d'affichage des résultats
* `.headers on` pour avoir le nom des attributs dans le résultat
* `.tables` pour avoir la liste de toutes les tables
* `.schema TABLE` pour avoir le schéma des ables

Sources
-------

* La base de SQL Murder Mystery est accessible sur la page github du site : https://github.com/NUKnightLab/sql-mysteries
