
DROP TABLE IF EXISTS Enseigne;
DROP TABLE IF EXISTS Inscrit;
DROP TABLE IF EXISTS UE;
DROP TABLE IF EXISTS Enseignant;
DROP TABLE IF EXISTS Etudiant;


CREATE TABLE Etudiant (
       NumEt INTEGER PRIMARY KEY,
       NomEt VARCHAR2(255),
       Adresse VARCHAR2(1000)
);
CREATE TABLE Enseignant (
       NumEns INTEGER PRIMARY KEY,
       NomEns VARCHAR2(255)
);
CREATE TABLE UE (
       NumUE INTEGER PRIMARY KEY,
       Titre VARCHAR2(255),
       HCours FLOAT,
       HTD FLOAT,
       HTP FLOAT
);
CREATE TABLE Enseigne (
       NumEns INTEGER REFERENCES Enseignant(NumEns),
       NumUE INTEGER REFERENCES UE(NumUE),
       NCours INTEGER,
       NTD INTEGER,
       NTP INTEGER,
       PRIMARY KEY (NumEns,NumUE)
);
CREATE TABLE Inscrit (
       NumEt INTEGER REFERENCES Etudiant(NumEt),
       NumUE INTEGER REFERENCES UE(NumUE),
       PRIMARY KEY (NumEt,NumUE)
);

INSERT INTO Etudiant(NumEt, NomEt, Adresse) VALUES ('1111', 'Armand A.', '1234 rue premiere');
INSERT INTO Etudiant(NumEt, NomEt, Adresse) VALUES ('1112', 'Berthe B.', '2345 rue deuxieme');
INSERT INTO Etudiant(NumEt, NomEt, Adresse) VALUES ('1113', 'Cendrine C.', '3456 rue troisieme');
INSERT INTO Etudiant(NumEt, NomEt, Adresse) VALUES ('1114', 'David D.', '4567 rue quatrieme');
INSERT INTO Etudiant(NumEt, NomEt, Adresse) VALUES ('1115', 'Erwan E.', '5678 rue cinquieme');
INSERT INTO Etudiant(NumEt, NomEt, Adresse) VALUES ('1116', 'Fabien F.', '6789 rue sixieme');
INSERT INTO Etudiant(NumEt, NomEt, Adresse) VALUES ('1117', 'Gerald G.', '7890 rue septieme');
INSERT INTO Etudiant(NumEt, NomEt, Adresse) VALUES ('1118', 'Herbert H.', '8901 rue huitieme');
INSERT INTO Etudiant(NumEt, NomEt, Adresse) VALUES ('1119', 'Jacques J.', '9012 rue neuvieme');

INSERT INTO Enseignant VALUES (111,'Albert A.');
INSERT INTO Enseignant VALUES (112,'Bertrand B.');
INSERT INTO Enseignant VALUES (113,'Carine C.');
INSERT INTO Enseignant VALUES (114,'David D.');
INSERT INTO Enseignant VALUES (115,'Edgar E.');

INSERT INTO UE VALUES (1,'Analyse',20,25,0);
INSERT INTO UE VALUES (2,'Algebre',20,25,0);
INSERT INTO UE VALUES (3,'Programmation',15,15,15);
INSERT INTO UE VALUES (4,'Algorithmique',20,15,15);
INSERT INTO UE VALUES (5,'Bases de donnes',18,18,18);
INSERT INTO UE VALUES (6,'Reseaux',6,0,2);

INSERT INTO Enseigne VALUES (111,1,1,1,0);
INSERT INTO Enseigne VALUES (111,2,0,1,0);
INSERT INTO Enseigne VALUES (112,1,0,1,0);
INSERT INTO Enseigne VALUES (112,2,1,1,0);
INSERT INTO Enseigne VALUES (113,3,1,1,1);
INSERT INTO Enseigne VALUES (114,4,1,1,1);
INSERT INTO Enseigne VALUES (115,5,1,1,1);
INSERT INTO Enseigne VALUES (113,4,0,0,1);
INSERT INTO Enseigne VALUES (113,5,0,1,1);
INSERT INTO Enseigne VALUES (114,3,0,0,1);
INSERT INTO Enseigne VALUES (114,5,0,1,1);
INSERT INTO Enseigne VALUES (115,3,0,0,1);
INSERT INTO Enseigne VALUES (115,4,0,1,1);

INSERT INTO Inscrit VALUES(1111,5);
INSERT INTO Inscrit VALUES(1111,4);
INSERT INTO Inscrit VALUES(1111,3);
INSERT INTO Inscrit VALUES(1112,4);
INSERT INTO Inscrit VALUES(1112,5);
INSERT INTO Inscrit VALUES(1114,6);
INSERT INTO Inscrit VALUES(1114,1);
INSERT INTO Inscrit VALUES(1114,2);
INSERT INTO Inscrit VALUES(1114,3);
INSERT INTO Inscrit VALUES(1115,4);
INSERT INTO Inscrit VALUES(1116,5);
INSERT INTO Inscrit VALUES(1116,6);
INSERT INTO Inscrit VALUES(1117,1);
INSERT INTO Inscrit VALUES(1117,2);
INSERT INTO Inscrit VALUES(1117,3);
INSERT INTO Inscrit VALUES(1117,4);
INSERT INTO Inscrit VALUES(1118,5);
INSERT INTO Inscrit VALUES(1118,6);
INSERT INTO Inscrit VALUES(1119,1);
INSERT INTO Inscrit VALUES(1119,2);
INSERT INTO Inscrit VALUES(1119,3);




