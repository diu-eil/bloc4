DROP TABLE IF EXISTS Rating;
DROP TABLE IF EXISTS Reviewer;
DROP TABLE IF EXISTS Movie;


CREATE TABLE Reviewer (
  rID integer,
  name varchar(30) NOT NULL,
  PRIMARY KEY( rID )
);

CREATE TABLE Movie (
  mID integer,
  title varchar(30) NOT NULL,
  year integer,
  director varchar(30),
  PRIMARY KEY( mID )
);

CREATE TABLE Rating (
  rID integer,
  mID integer,
  stars integer NOT NULL,
  ratingdate date,

  PRIMARY KEY(rID,mID,stars),
  CONSTRAINT c1 FOREIGN KEY(rID) REFERENCES Reviewer,
  CONSTRAINT c2 FOREIGN KEY(mID) REFERENCES Movie
  
);


--Exercice 1.2
ALTER TABLE Movie ADD CONSTRAINT c3 UNIQUE(title,year);

-- Exercice 1.3
insert into Movie values(101, 'Gone with the Wind', 1939, 'Victor Fleming');
insert into Movie values(102, 'Star Wars', 1977, 'George Lucas');
insert into Movie values(103, 'The Sound of Music', 1965, 'Robert Wise');
insert into Movie values(104, 'E.T.', 1982, 'Steven Spielberg');
insert into Movie values(105, 'Titanic', 1997, 'James Cameron');
insert into Movie values(106, 'Snow White', 1937, null);
insert into Movie values(107, 'Avatar', 2009, 'James Cameron');
insert into Movie values(108, 'Raiders of the Lost Ark', 1981, 'Steven Spielberg');

insert into Reviewer values(201, 'Sarah Martinez');
insert into Reviewer values(202, 'Daniel Lewis');
insert into Reviewer values(203, 'Brittany Harris');
insert into Reviewer values(204, 'Mike Anderson');
insert into Reviewer values(205, 'Chris Jackson');
insert into Reviewer values(206, 'Elizabeth Thomas');
insert into Reviewer values(207, 'James Cameron');
insert into Reviewer values(208, 'Ashley White');

INSERT INTO Rating VALUES(201, 101, 2, to_date('22-01-2011', 'dd-mm-yyyy'));
insert into Rating values(201, 101, 4, to_date('27-01-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(202, 106, 4, to_date('04-02-2021', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(203, 103, 2, to_date('20-01-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(203, 108, 4, to_date('12-01-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(203, 108, 2, to_date('30-01-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(204, 101, 3, to_date('09-01-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(205, 103, 3, to_date('27-01-2011', 'dd-mm-yyyy'));
insert into Rating values(205, 104, 2, to_date('22-01-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(205, 108, 4, to_date('27-01-2012', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(206, 107, 3, to_date('15-01-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(206, 106, 5, to_date('19-01-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(207, 107, 5, to_date('20-01-2011', 'dd-mm-yyyy'));
insert into Rating values(208, 104, 3, to_date('02-01-2011', 'dd-mm-yyyy'));

INSERT INTO Rating VALUES(203, 102, 3, to_date('27-01-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(203, 101, 2, to_date('27-02-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(203, 104, 2, to_date('27-03-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(203, 105, 2, to_date('27-04-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(203, 106, 4, to_date('27-05-2011', 'dd-mm-yyyy'));
INSERT INTO Rating VALUES(203, 107, 5, to_date('27-06-2011', 'dd-mm-yyyy'));

-- Exercice 2 
----------------------------------

--1
SELECT name 
FROM Reviewer 
WHERE rID = 205;

--2
SELECT title 
FROM Movie;

--3
SELECT title 
FROM Movie 
ORDER BY title;

--4
SELECT title 
FROM Movie 
WHERE director='Steven Spielberg';

--5
SELECT title 
FROM Movie 
WHERE director IS NULL;

--6
SELECT DISTINCT year 
FROM Movie JOIN Rating ON Movie.mID=Rating.mID
WHERE stars > 3 
ORDER BY year;

--7
SELECT DISTINCT name 
FROM Rating JOIN Movie ON Movie.mID=Rating.mID 
            JOIN Reviewer ON Reviewer.rID = Rating.rID 
WHERE title='Gone with the Wind';

--8
SELECT name, title, stars 
FROM Rating JOIN Movie ON Movie.mID=Rating.mID 
            JOIN Reviewer ON Reviewer.rID = Rating.rID 
WHERE name = director;

--9

SELECT stars, name, title 
FROM Rating JOIN Movie ON Movie.mID=Rating.mID 
            JOIN Reviewer ON Reviewer.rID = Rating.rID 
ORDER BY name, title, stars;

--10

SELECT title 
FROM (
    SELECT mID, title 
    FROM Movie 
    EXCEPT
    SELECT Movie.mID, title
    FROM Rating JOIN Movie ON Movie.mID=Rating.mID 
                JOIN Reviewer ON Reviewer.rID = Rating.rID 
    WHERE name='Chris Jackson'
) t;

--11

SELECT name, title 
FROM Rating R1 JOIN Movie ON Movie.mID=R1.mID 
                JOIN Reviewer ON Reviewer.rID = R1.rID
                JOIN Rating R2 ON R1.mID = R2.mID AND R1.rID = R2.rID
WHERE R1.stars < R2.stars AND R1.ratingdate < R2.ratingdate;

--Exercice 3
----------------------------------

-- 3.1

SELECT title 
FROM (
    SELECT mID, title 
    FROM Movie 
    EXCEPT
    SELECT Movie.mID, title
    FROM Rating JOIN Movie ON Movie.mID=Rating.mID 
                JOIN Reviewer ON Reviewer.rID = Rating.rID 
) t;

-- 3.2

SELECT name, title, stars
FROM Rating JOIN Movie ON Movie.mID=Rating.mID 
                JOIN Reviewer ON Reviewer.rID = Rating.rID 
WHERE stars = (SELECT MIN(stars)
                FROM Rating);

-- 3.3

SELECT DISTINCT title, stars
FROM Rating JOIN Movie m ON m.mID=Rating.mID 
WHERE stars = (SELECT MAX(stars) 
                FROM Rating R2
                WHERE R2.mID=m.mID)
ORDER BY title;

-- 3.4

SELECT DISTINCT name
FROM Reviewer
WHERE (SELECT count(distinct mID)
        FROM Rating
        WHERE Rating.rID=Reviewer.rID)
        =
        (SELECT count(mID)
        FROM Movie);
         

-- Exercice 4
----------------------------------

-- 4.1

select 	title as "Titre", 
		avg(stars) as "Note Moyenne", 
		max(stars) as "Note max", 
		min(stars) as "Note min"
from reviewer r join rating r2 using (rID) 
                join movie m2 using (mID)
group by(m2.mid, title);

-- 4.2

select name as "Rapporteur"
from reviewer r join rating r2 using (rID) 
group by(r.rID, name)
    having count(r2.mid)>=3;

-- 4.3

select  title       as "Film",
		    avg(stars)  as "Note moyenne"
from rating r1 join movie m1 using(mid) 
group by m1.mid,title
    having avg(stars)= (select avg(stars)
              from rating r2
              group by r2.mid	
              order by avg(stars) desc
              limit 1);

-- 4.4

select title as "Film",
		avg(stars) as "Note moyenne"
from rating r1 join movie m1 using(mid) 
group by m1.mid,title
    having avg(stars)= (select avg(stars)
					              from rating r2
                        group by r2.mid	
                        order by avg(stars) 
                        limit 1);

-- 4.5

select (avg("moyenne_par_film_av_1980") - avg("moyenne_par_film_ap_1980")) as "Différence"
from 	(	select avg(stars) as "moyenne_par_film_av_1980"
			from rating r2 join movie m2 using (mid)
			where to_date(to_char(year,'9999'),'yyyy') <= to_date('1980', 'yyyy')
			group by r2.mid ) t_1, -- produit cartésien entre deux relations à un seul tuple et un seul attribut
		(	select avg(stars) as "moyenne_par_film_ap_1980"
			from rating r2 join movie m2 using (mid)
			where to_date(to_char(year,'9999'),'yyyy') >= to_date('1980', 'yyyy')
			group by r2.mid ) t_2;

-- 4.6

select 	title as "titre",
		    (max(stars)-min(stars)) as "difference"
from rating r1 join movie m1 using(mid) 
group by (mid, title)
order by "titre";


-- Exercice 5
----------------------------------

-- 5.1

select  title, 
        avg(stars), 
        rank() over (order by avg(stars) desc)
from reviewer r join rating r2 using (rID) 
                join movie m2 using (mID)
group by m2.mid;

-- 5.2

select distinct name, 
				count(*) over w as "Nombre de films rapportés", 
				avg(stars) over w as "Moyenne des notes", 
				max(stars) over w as "Note maximale", 
				min(stars) over w as "Note minimale"
from reviewer r join rating r2 using (rID)
window w as (partition by rid)
order by "Nombre de films rapportés";

-- Note : cette requête peut se faire avec un simple group by...


-- 5.3
select distinct name, 
                title, 
                max(stars) over (partition by rid,mid order by stars desc),
                avg(stars) over (partition by mid)
from reviewer r join rating r2 using (rID) 
                join movie m2 using (mID)
order by name,title;



-- Exercice 6
----------------------------------

-- 6.1

select  name as "rapporteur", 
        title as "Titre", 
        avg(stars) as "Moyenne"
from reviewer r join rating r2 using (rID) 
                join movie m2 using (mID)
group by grouping sets ((r.rID),(r.rid,m2.mID,title))
order by "Rapporteur", "Titre", "Moyenne";

-- 6.2

select name as "Rapporteur", 
      coalesce(title, ' TOUS LES FILMS') as "Titre", 
      avg(stars) as "Moyenne"
from reviewer r join rating r2 using (rID) 
                join movie m2 using (mID)
group by grouping sets ((r.rID),(r.rid,m2.mID,title))
order by "Rapporteur", "Titre", "Moyenne";

-- 6.3

select 	coalesce(name, ' TOUS LES RAPPORTEURS') as "RAPPORTEUR", 
		    coalesce(title, ' TOUS LES FILMS') as "FILM", 
		    coalesce(to_char(ratingdate,'yyyy'),' TOUTES LES ANNEES') as "ANNEE",
		    avg(stars)
from reviewer r join rating r2 using (rID) 
                join movie m2 using (mID)
group by cube ((rid, name), (mid, title), to_char(ratingdate,'yyyy'))
order by "RAPPORTEUR", "FILM", "ANNEE";