DIU bloc 4 : "Bases de données : introduction et SQL"
====================================================

Objectifs pédagogiques
----------------------

Extrait du programme du DIU :

> Structuration de l'information
> * Notion informelle de SGBD (Système de Gestion de Bases de Données)
> * Bases de données relationnelles, modèle relationnel, schémas, tables et relations
> * Contraintes des bases de données relationnelles : clés primaires, étrangères
>
> Langages de données et d'interrogation
> * Langage de requête : SQL
> * Programmation web côté serveur et interrogation d'une base de données

Contenus/programmes
-------------------

### Le cours 

Une introduction aux SGBDs et à SQL

* Les [slides](IntroductionBD.pdf)
* La [vidéo YT de la partie 1](https://youtu.be/fWR3wfO3V50) "SGBD et modèle relationnel"
* La [vidéo YT de la partie 2](https://youtu.be/Q3Cg18FRHB0) "Les bases de SQL"
* La [vidéo YT de prise en main de SQLite](https://youtu.be/MxyE7rnta5w)
    

### Travailler le SQl

Vous pouvez pratiquer avec les deux activités suivantes en autonomie : 

* <https://mystery.knightlab.com/> : une enquête en ligne
  * une [copie de la base est dans le dépot](TP/base-sql-murder-mystery.db)
* une base jouet avec quelques requêtes
  * [sujet](TP/base-etudiant-questions.pdf)
  * [BD Sqlite](TP/base-etudiant.db) et [script SQL](TP/base-etudiant.sql)

Mardi 8 juin, nous ferons ensemble [ce TP](TP/TP_SQL1.pdf) qui aborde la création de tables et de contraintes simples, ainsi que des requêtes simple et un peu plus avancées.
  
### Correction des activités autonomes

* [Vidéo YT de correction](https://youtu.be/XxJRKd7WMvA) sur la base étudiant
* [fichiers dans la branche correction du GitLab](https://forge.univ-lyon1.fr/diu-eil/bloc4/-/tree/correction/3_bases_de_donnees_introduction/TP/correction)

### Séances du mardi 8 juin

 * Le cours sera [en visio WEBEX](
    https://univ-lyon1.webex.com/univ-lyon1/j.php?MTID=m15867747e48770cf7232e098e469ba7e)(Cliquez pour rejoindre la réunion)

### Références
-----------------------

* La documentation de SQLite <https://www.sqlite.org/docs.html>
* Un tutorial pour SQLite <https://www.sqlitetutorial.net/>
* Un mémento SQL <https://codi-lyon.beta.education.fr/GMedgMDXQBaXHMhlErJLPw#>
* [Un très bon site pour apprendre et s'exercer en SQL](https://pgexercises.com/)
